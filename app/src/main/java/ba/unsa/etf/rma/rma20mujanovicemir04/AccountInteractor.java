package ba.unsa.etf.rma.rma20mujanovicemir04;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class AccountInteractor extends AsyncTask<Double, Integer, Void> {

    private String ROOT = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com";
    private String api_id;

    private Context context;
    private OnAccountUpdateDone caller;

    private Account account;
    private boolean get = true;
    private double budget;
    private double totalLimit;
    private double monthLimit;
    private boolean parameterFour = false;

    public interface OnAccountUpdateDone{
        public void onAccountDone();
        public void continueUpdate(double budget, double totalLimit, double monthLimit);
    }

    public AccountInteractor(Context context) {
        this.context = context;
        api_id = context.getResources().getString(R.string.api_id);
    }

    public AccountInteractor(Context context, OnAccountUpdateDone p) {
        this.caller = p;
        this.context = context;
        api_id = context.getResources().getString(R.string.api_id);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if(account!=null){
            set(account);
        }

        if(get){
            caller.onAccountDone();
        }else{
            if(parameterFour){
                getAccount().setBudget(getAccount().getBudget() + budget);
                getAccount().setTotalLimit(totalLimit);
                getAccount().setMonthLimit(monthLimit);

                caller.continueUpdate(getAccount().getBudget(), getAccount().getTotalLimit(), getAccount().getMonthLimit());
            }
        }
    }

    @Override
    protected Void doInBackground(Double... doubles) {

        if(doubles.length==0){
            get = true;
            webServiceGET();
        }else if(doubles.length==3){
            get = false;
            webServicePOST(doubles[0], doubles[1], doubles[2]);
        }else if(doubles.length==4){
            get = false;
            parameterFour = true;
            budget = doubles[0];
            totalLimit = doubles[1];
            monthLimit = doubles[2];
            webServiceGET();
        }

        return null;
    }

    public Account getAccount(){
        return AccountModel.account;
    }

    /*
    public Account get(){
        if(AccountModel.account == null){
            AccountModel.addAccountData();
        }
        return AccountModel.account;
    }
    */

    public void set(Account account){
        AccountModel.account = account;
    }

    public Cursor getAccountCursor() {
        ContentResolver cr = context.getApplicationContext().getContentResolver();
        String[] kolone = new String[]{
                TransactionDBOpenHelper.ACCOUNT_ID,
                TransactionDBOpenHelper.ACCOUNT_BUDGET,
                TransactionDBOpenHelper.ACCOUNT_TOTAL_LIMIT,
                TransactionDBOpenHelper.ACCOUNT_MONTH_LIMIT
        };
        Uri adresa = ContentUris.withAppendedId(Uri.parse("content://rma.provider.account/elements"),42);
        String where = null;
        String whereArgs[] = null;
        String order = null;
        Cursor cur = cr.query(adresa,kolone,where,whereArgs,order);
        return cur;
    }

    private void webServiceGET(){
        try {
            String url = ROOT + "/account/" + api_id;

            URL objectURL = new URL(url);
            HttpURLConnection urlConnection = null;
            urlConnection = (HttpURLConnection) objectURL.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String result = convertStreamToString(in);

            JSONObject res = new JSONObject(result);

            double budget = res.getDouble("budget");
            double monthLimit = res.getDouble("monthLimit");
            double totalLimit = res.getDouble("totalLimit");

            account = new Account(budget, totalLimit, monthLimit);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void webServicePOST(double budget, double totalLimit, double monthLimit){
        try {
            String url = ROOT + "/account/" + api_id;

            URL objectURL = new URL(url);
            HttpURLConnection urlConnection = null;
            urlConnection = (HttpURLConnection) objectURL.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            urlConnection.setRequestProperty("Accept", "application/json");

            urlConnection.setDoOutput(true);

            String jsonInputString = "{\"budget\":" + budget + ",\"totalLimit\":" +  totalLimit +
                    ",\"monthLimit\":" + monthLimit + "}";

            try(OutputStream os = urlConnection.getOutputStream()) {
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }


            try(BufferedReader br = new BufferedReader(
                    new InputStreamReader(urlConnection.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                //System.out.println("RESPONSE" + response.toString());
            }

            account = new Account(budget, totalLimit, monthLimit);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String convertStreamToString(InputStream in) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                in.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }
}
