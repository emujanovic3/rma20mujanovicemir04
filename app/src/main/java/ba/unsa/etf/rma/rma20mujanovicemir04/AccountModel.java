package ba.unsa.etf.rma.rma20mujanovicemir04;

public class AccountModel {
    static Account account = new Account(0, 1000, 100);

    public AccountModel() {
    }

    public static void addAccountData() {
        account = new Account(0,1000,100);

        //Ova ažuriranja accounta odgovaraju svakoj od 20 hardkodiranih transakcija
        account.addToBudget(100);
        account.addToBudget(-50);
        account.addToBudget(-30);
        account.addToBudget(1000);
        account.addToBudget(-120);
        account.addToBudget(250);
        account.addToBudget(-600);
        account.addToBudget(-100);
        account.addToBudget(-150);
        account.addToBudget(-1000);
        account.addToBudget(150);
        account.addToBudget(-200);
        account.addToBudget(500);
        account.addToBudget(-100);
        account.addToBudget(-50);
        account.addToBudget(-1);
        account.addToBudget(-15);
        account.addToBudget(500);
        account.addToBudget(1000);
    }
}
