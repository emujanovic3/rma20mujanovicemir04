package ba.unsa.etf.rma.rma20mujanovicemir04;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;

public class AccountPresenter implements AccountInteractor.OnAccountUpdateDone{
    private Context context;
    private AccountInteractor interactor;

    public AccountPresenter(Context context, AccountInteractor interactor) {
        this.context = context;
        this.interactor = interactor;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public AccountInteractor getInteractor() {
        return interactor;
    }


    public void setInteractor(AccountInteractor interactor) {
        this.interactor = interactor;
    }

    public void updateAccount() {
        if(ConnectivityBroadcastReceiver.isConnected()){
            new AccountInteractor(context, (AccountInteractor.OnAccountUpdateDone)this).execute();
        }
    }

    public void updateAccount(double budget, double totalLimit, double monthLimit) {
        updateAccountInDB(budget, totalLimit, monthLimit);
        getInteractor().set(new Account(budget, totalLimit, monthLimit));

        if(ConnectivityBroadcastReceiver.isConnected()){
            if(TransactionListFragment.noAccountInDB){
                TransactionListFragment.noAccountInDB = false;
                new AccountInteractor(context, (AccountInteractor.OnAccountUpdateDone)this).execute(budget, totalLimit, monthLimit, 1.0);
            }else{
                new AccountInteractor(context, (AccountInteractor.OnAccountUpdateDone)this).execute(budget,totalLimit,monthLimit);
            }

        }
    }

    private void updateAccountInDB(double budget, double totalLimit, double monthLimit) {
        ContentResolver cr = context.getApplicationContext().getContentResolver();
        Uri uri = ContentUris.withAppendedId(Uri.parse("content://rma.provider.account/elements"),42);

        cr.delete(uri,null,null);

        uri = Uri.parse("content://rma.provider.account/elements");

        ContentValues contentValues = new ContentValues();
        contentValues.put(TransactionDBOpenHelper.ACCOUNT_ID, 42);
        contentValues.put(TransactionDBOpenHelper.ACCOUNT_BUDGET, budget);
        contentValues.put(TransactionDBOpenHelper.ACCOUNT_TOTAL_LIMIT, totalLimit);
        contentValues.put(TransactionDBOpenHelper.ACCOUNT_MONTH_LIMIT, monthLimit);

        cr.insert(uri,contentValues);
    }

    @Override
    public void onAccountDone() {
        MainActivity mainActivity = (MainActivity) context;
        mainActivity.onAccountDone();
    }

    @Override
    public void continueUpdate(double budget, double totalLimit, double monthLimit){
        new AccountInteractor(context, (AccountInteractor.OnAccountUpdateDone)this).execute(budget,totalLimit,monthLimit);
    }
}
