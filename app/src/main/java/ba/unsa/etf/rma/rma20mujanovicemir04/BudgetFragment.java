package ba.unsa.etf.rma.rma20mujanovicemir04;

import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

public class BudgetFragment extends Fragment {
    private AccountPresenter accountPresenter;

    private ConstraintLayout budgetLayout;
    private TextView budget;
    private EditText totalLimit;
    private EditText monthLimit;
    private Button saveChangesBtn;
    private TextView budgetOfflineTextView;

    private TransactionListFragment.OnRightSwipe ors;
    private TransactionListFragment.OnLeftSwipe ols;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.budget_layout,container,false);

        accountPresenter = new AccountPresenter(getContext(),new AccountInteractor(getContext()));

        budgetLayout = (ConstraintLayout)view.findViewById(R.id.budgetLayout);
        budget = (TextView)view.findViewById(R.id.budget);
        totalLimit = (EditText)view.findViewById(R.id.totalLimit);
        monthLimit = (EditText)view.findViewById(R.id.monthLimit);
        saveChangesBtn = (Button)view.findViewById(R.id.saveChangesBtn);
        budgetOfflineTextView = (TextView)view.findViewById(R.id.budgetOfflineTextView);

        try {
            ors = (TransactionListFragment.OnRightSwipe) getActivity();
            ols = (TransactionListFragment.OnLeftSwipe) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() +
                    "Treba implementirati OnRightSwipe");
        }

        updateBudget();

        if(!ConnectivityBroadcastReceiver.isConnected()){
            enableOfflineText();
        }

        totalLimit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                totalLimit.setBackgroundColor(getResources().getColor(R.color.colorGreen));
            }
        });

        monthLimit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                monthLimit.setBackgroundColor(getResources().getColor(R.color.colorGreen));
            }
        });

        saveChangesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearBackground();
                accountPresenter.updateAccount(Double.parseDouble(budget.getText().toString()),
                        Double.parseDouble(totalLimit.getText().toString()),
                        Double.parseDouble(monthLimit.getText().toString()));
            }
        });

        budgetLayout.setOnTouchListener(new OnSwipeTouchListener(getActivity().getApplicationContext()) {
            public void onSwipeTop() {
            }
            public void onSwipeRight() { ;
                ors.onRightSwiped(BudgetFragment.this);
            }
            public void onSwipeLeft() {
                ols.onLeftSwiped(BudgetFragment.this);
            }
            public void onSwipeBottom() {
            }
        });

        return view;
    }

    public void enableOfflineText(){
        budgetOfflineTextView.setText("Offline\nedit");
    }

    public void disableOfflineText(){
        budgetOfflineTextView.setText("");
    }

    private void updateBudget() {
        if(ConnectivityBroadcastReceiver.isConnected()){
            accountPresenter.updateAccount();
        }else{
            Cursor cursor = accountPresenter.getInteractor().getAccountCursor();

            if(cursor.getCount()!=0){
                cursor.moveToFirst();

                int INDEX_BUDGET = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.ACCOUNT_BUDGET);
                int INDEX_TOTAL_LIMIT = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.ACCOUNT_TOTAL_LIMIT);
                int INDEX_MONTH_LIMIT = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.ACCOUNT_MONTH_LIMIT);

                accountPresenter.getInteractor().set(new Account(cursor.getDouble(INDEX_BUDGET), cursor.getDouble(INDEX_TOTAL_LIMIT),
                        cursor.getDouble(INDEX_MONTH_LIMIT)));
            }

            refreshBudget();
        }

    }

    public void refreshBudget(){
        budget.setText(Double.toString(accountPresenter.getInteractor().getAccount().getBudget()));
        totalLimit.setText(Double.toString(accountPresenter.getInteractor().getAccount().getTotalLimit()));
        monthLimit.setText(Double.toString(accountPresenter.getInteractor().getAccount().getMonthLimit()));
        clearBackground();
    }

    private void clearBackground(){
        totalLimit.setBackgroundColor(getResources().getColor(R.color.colorBackgroundLight));
        monthLimit.setBackgroundColor(getResources().getColor(R.color.colorBackgroundLight));
    }
}
