package ba.unsa.etf.rma.rma20mujanovicemir04;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import java.util.Calendar;

public class ConnectivityBroadcastReceiver extends BroadcastReceiver {
    private static boolean connected = false;
    private static boolean firstTime = true;
    private static boolean recieved = false;
    private static MainActivity mainActivity;

    public ConnectivityBroadcastReceiver(){

    }

    public ConnectivityBroadcastReceiver(MainActivity mainActivity){
        this.mainActivity = mainActivity;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);


        if (cm.getActiveNetworkInfo() == null) {
            Toast toast = Toast.makeText(context, "Disconnected", Toast.LENGTH_SHORT);
            toast.show();
            connected = false;

            updateOfflineTextView();
        }
        else {
            Toast toast = Toast.makeText(context, "Connected", Toast.LENGTH_SHORT);
            toast.show();
            connected = true;
            /*
            if(firstTime){
                firstTime = false;
            }else{
                updateWebService(context);
            }

            */
            updateWebService(context);
            updateTransactionlist();
            clearOfflineTextView();
        }

        if(recieved==false){
            Cursor cursor = new AccountInteractor(context).getAccountCursor();

            if(cursor.getCount()==0 && connected==false){
                TransactionListFragment.noAccountInDB = true;
            }
        }

        recieved = true;
    }

    private void clearOfflineTextView() {
        Fragment fragment = mainActivity.getSupportFragmentManager().findFragmentById(R.id.transaction_list);

        if(fragment instanceof BudgetFragment){
            ((BudgetFragment) fragment).disableOfflineText();
            return;
        }

        if(fragment instanceof TransactionDetailFragment){
            ((TransactionDetailFragment) fragment).clearOfflineTextView();
        }else{
            fragment = mainActivity.getSupportFragmentManager().findFragmentById(R.id.transaction_detail);

            if(fragment instanceof TransactionDetailFragment){
                ((TransactionDetailFragment) fragment).clearOfflineTextView();
            }
        }
    }

    private void updateOfflineTextView() {
        Fragment fragment = mainActivity.getSupportFragmentManager().findFragmentById(R.id.transaction_list);

        if(fragment instanceof BudgetFragment){
             ((BudgetFragment) fragment).enableOfflineText();
             return;
        }

        if(fragment instanceof TransactionDetailFragment){
            ((TransactionDetailFragment) fragment).updateOfflineTextView();
        }else{
            fragment = mainActivity.getSupportFragmentManager().findFragmentById(R.id.transaction_detail);

            if(fragment instanceof TransactionDetailFragment){
                ((TransactionDetailFragment) fragment).updateOfflineTextView();
            }
        }
    }

    private void updateTransactionlist() {
        Fragment fragment = mainActivity.getSupportFragmentManager().findFragmentById(R.id.transaction_list);
        if(fragment instanceof TransactionListFragment){
            ((TransactionListFragment) fragment).refresh();
        }
    }

    public static boolean isConnected(){
        return connected;
    }

    public static boolean isRecieved(){
        return recieved;
    }

    private void updateWebService(Context context){
        TransactionListPresenter transactionListPresenter = new TransactionListPresenter(context, new TransactionListInteractor(context));
        Cursor cursor = new TransactionListInteractor(context).getTransactionsCursor();

        int INDEX_ID = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ID);
        int INDEX_DATE = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_DATE);
        int INDEX_AMOUNT = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_AMOUNT);
        int INDEX_TITLE = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TITLE);
        int INDEX_TYPE = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TYPE);
        int INDEX_DESCRIPTION = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_DESCRIPTION);
        int INDEX_INTERVAL = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_INTERVAL);
        int INDEX_END_DATE = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_END_DATE);
        int INDEX_MODE = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_MODE);


        while(cursor.moveToNext()){
            int id = cursor.getInt(INDEX_ID);
            Calendar date = dateFromString(cursor.getString(INDEX_DATE));
            double amount = cursor.getDouble(INDEX_AMOUNT);
            String tite = cursor.getString(INDEX_TITLE);

            int typeInt = cursor.getInt(INDEX_TYPE);
            TransactionType type = getTypeFromId(typeInt);

            String description = cursor.getString(INDEX_DESCRIPTION);
            int interval = cursor.getInt(INDEX_INTERVAL);
            Calendar endDate = null;

            String endDateString = cursor.getString(INDEX_END_DATE);

            if(!(endDateString.equals(""))){
                endDate = dateFromString(endDateString);
            }

            int mode = cursor.getInt(INDEX_MODE);

            Transaction t = new Transaction(date,amount,tite,type,description,interval,endDate);
            t.setId(id);

            if(mode==TransactionDBOpenHelper.TRANSACTION_ADDED || mode==TransactionDBOpenHelper.TRANSACTION_EDITED){
                transactionListPresenter.addOrEditTransaction(t);
            }else if(mode==TransactionDBOpenHelper.TRANSACTION_DELETED){
                transactionListPresenter.deleteTransaction(t);
            }
        }

        transactionListPresenter.deleteAllTransactionsFromDB();
        refreshTransactionList();

        AccountPresenter accountPresenter = new AccountPresenter(context, new AccountInteractor(context));
        cursor = accountPresenter.getInteractor().getAccountCursor();

        if(cursor.getCount()!=0){
            cursor.moveToFirst();

            int INDEX_BUDGET = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.ACCOUNT_BUDGET);
            int INDEX_TOTAL_LIMIT = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.ACCOUNT_TOTAL_LIMIT);
            int INDEX_MONTH_LIMIT = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.ACCOUNT_MONTH_LIMIT);

            accountPresenter.updateAccount(cursor.getDouble(INDEX_BUDGET), cursor.getDouble(INDEX_TOTAL_LIMIT), cursor.getDouble(INDEX_MONTH_LIMIT));
        }
    }

    private void refreshTransactionList() {
        //NE ZNAM STO OVO RADIM
    }

    private Calendar dateFromString(String s){
        String[] spl = s.split("\\.");
        int day = Integer.parseInt(spl[0]);
        int month = Integer.parseInt(spl[1]) - 1;
        int year = Integer.parseInt(spl[2]);

        Calendar c = Calendar.getInstance();
        c.set(year,month,day);
        return c;
    }

    private TransactionType getTypeFromId(int transactionTypeId) {
        while(TransactionType.transactionTypeHashMap==null){
            // Da malo sacekam ako bude trebalo cekati da se transakcijski tipovi pokupe sa web servisa
            // Jer se GET prvo radi, pa je moguce da uhavtim hashmapu dok je null
        }
        return TransactionType.transactionTypeHashMap.get(transactionTypeId);
    }
}