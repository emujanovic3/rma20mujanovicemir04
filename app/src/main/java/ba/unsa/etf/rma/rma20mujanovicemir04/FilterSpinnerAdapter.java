package ba.unsa.etf.rma.rma20mujanovicemir04;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

public class FilterSpinnerAdapter extends ArrayAdapter<String> {
    private Context context;
    private int resource;

    public FilterSpinnerAdapter(@NonNull Context context, int resource, @NonNull List<String> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        String transactionType = getItem(position);

        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(resource,parent,false);

        ImageView filterIcon = (ImageView) convertView.findViewById(R.id.filterIcon);
        TextView filterTitle = (TextView) convertView.findViewById(R.id.filterTitle);


        if(transactionType.equals("Purchase")){
            filterIcon.setImageResource(R.drawable.purchase);
            filterTitle.setText("Purchase");
        }else if(transactionType.equals("Individual income")){
            filterIcon.setImageResource(R.drawable.individualincome);
            filterTitle.setText("Individual income");
        }else if(transactionType.equals("Individual payment")){
            filterIcon.setImageResource(R.drawable.individualpayment);
            filterTitle.setText("Individual payment");
        }else if(transactionType.equals("Regular income")){
            filterIcon.setImageResource(R.drawable.regularincome);
            filterTitle.setText("Regular income");
        }else if(transactionType.equals("Regular payment")){
            filterIcon.setImageResource(R.drawable.regularpayment);
            filterTitle.setText("Regular payment");
        }else{
            filterIcon.setImageResource(R.drawable.all_types);
            filterTitle.setText("All types");
        }

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        String transactionType = getItem(position);

        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(resource,parent,false);

        ImageView filterIcon = (ImageView) convertView.findViewById(R.id.filterIcon);
        TextView filterTitle = (TextView) convertView.findViewById(R.id.filterTitle);


        if(transactionType.equals("Purchase")){
            filterIcon.setImageResource(R.drawable.purchase);
            filterTitle.setText("Purchase");
        }else if(transactionType.equals("Individual income")){
            filterIcon.setImageResource(R.drawable.individualincome);
            filterTitle.setText("Individual income");
        }else if(transactionType.equals("Individual payment")){
            filterIcon.setImageResource(R.drawable.individualpayment);
            filterTitle.setText("Individual payment");
        }else if(transactionType.equals("Regular income")){
            filterIcon.setImageResource(R.drawable.regularincome);
            filterTitle.setText("Regular income");
        }else if(transactionType.equals("Regular payment")){
            filterIcon.setImageResource(R.drawable.regularpayment);
            filterTitle.setText("Regular payment");
        }else{
            filterIcon.setImageResource(R.drawable.all_types);
            filterTitle.setText("All types");
        }

        return convertView;
    }
}
