package ba.unsa.etf.rma.rma20mujanovicemir04;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;
import java.util.Calendar;

public class GraphsFragment extends Fragment {
    private ConstraintLayout graphsLayout;
    private RadioGroup radioGroup;
    private RadioButton radioButtonMonth;

    private BarChart barChartConsumption;
    private BarChart barChartEarnings;
    private BarChart barChartTotal;

    private TransactionListFragment.OnRightSwipe ors;
    private TransactionListFragment.OnLeftSwipe ols;

    private TransactionListPresenter transactionListPresenter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.graphs_layout,container,false);

        transactionListPresenter = new TransactionListPresenter(getContext(),new TransactionListInteractor(getContext()));

        try {
            ors = (TransactionListFragment.OnRightSwipe) getActivity();
            ols = (TransactionListFragment.OnLeftSwipe) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() +
                    "Treba implementirati OnRightSwipe i OnLeftSwipe");
        }

        graphsLayout = (ConstraintLayout)view.findViewById(R.id.graphsLayout);
        radioGroup = (RadioGroup)view.findViewById(R.id.radioGroup);
        radioButtonMonth = (RadioButton)view.findViewById(R.id.radioButtonMonth);


        barChartConsumption = (BarChart) view.findViewById(R.id.barChartConsumption);
        barChartEarnings = (BarChart) view.findViewById(R.id.barChartEarnings);
        barChartTotal = (BarChart) view.findViewById(R.id.barChartTotal);

        barChartConsumption.getDescription().setEnabled(false);
        barChartEarnings.getDescription().setEnabled(false);
        barChartTotal.getDescription().setEnabled(false);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                updateList();
            }
        });

        radioButtonMonth.setChecked(true);

        graphsLayout.setOnTouchListener(new OnSwipeTouchListener(getActivity().getApplicationContext()) {
            public void onSwipeTop() {
            }
            public void onSwipeRight() { ;
                ors.onRightSwiped(GraphsFragment.this);
            }
            public void onSwipeLeft() {
                ols.onLeftSwiped(GraphsFragment.this);
            }
            public void onSwipeBottom() {
            }
        });

        return view;
    }

    public void updateGraphs(){
        RadioButton checkedRadioButton = (RadioButton)radioGroup.findViewById(radioGroup.getCheckedRadioButtonId());

        if(checkedRadioButton.getText().toString().equals(getString(R.string.day))){
            setDayConsumptionBarChart();
            setDayEarningsBarChart();
            setDayTotalBarChart();
        }else if(checkedRadioButton.getText().toString().equals(getString(R.string.week))){
            setWeekConsumptionBarChart();
            setWeekEarningsBarChart();
            setWeekTotalBarChart();
        }else if(checkedRadioButton.getText().toString().equals(getString(R.string.month))){
            setMonthConsumptionBarChart();
            setMonthEarningsBarChart();
            setMonthTotalBarChart();
        }
    }

    private void updateList() {
        transactionListPresenter.updateTransactions();
    }

    private void setWeekTotalBarChart() {
        BarDataSet totalBarDataSet = new BarDataSet(dataValueWeekTotal(),"Total");
        totalBarDataSet.setDrawValues(false);
        BarData totalBarData = new BarData();
        totalBarData.addDataSet(totalBarDataSet);
        barChartTotal.setData(totalBarData);
        barChartTotal.invalidate();
    }

    private ArrayList<BarEntry> dataValueWeekTotal() {
        ArrayList<BarEntry> dataVals = new ArrayList<>();
        int year = TransactionListFragment.calendar.get(Calendar.YEAR);
        int month = TransactionListFragment.calendar.get(Calendar.MONTH);

        double total = 0;

        for(int week = 1; week<=TransactionListFragment.calendar.getActualMaximum(Calendar.WEEK_OF_MONTH); week++){
            total = total + giveWeekEarnings(week,month,year) - giveWeekConsumption(week,month,year);
            dataVals.add(new BarEntry(week,(float)total));
        }

        return dataVals;
    }

    private void setWeekEarningsBarChart() {
        BarDataSet earningsBarDataSet = new BarDataSet(dataValueWeekEarnings(),"Earnings");
        earningsBarDataSet.setDrawValues(false);
        BarData earningsBarData = new BarData();
        earningsBarData.addDataSet(earningsBarDataSet);
        barChartEarnings.setData(earningsBarData);
        barChartEarnings.invalidate();
    }

    private ArrayList<BarEntry> dataValueWeekEarnings() {
        ArrayList<BarEntry> dataVals = new ArrayList<>();
        int year = TransactionListFragment.calendar.get(Calendar.YEAR);
        int month = TransactionListFragment.calendar.get(Calendar.MONTH);

        for(int week = 1; week<=TransactionListFragment.calendar.getActualMaximum(Calendar.WEEK_OF_MONTH); week++){
            dataVals.add(new BarEntry(week,(float)giveWeekEarnings(week,month,year)));
        }

        return dataVals;
    }

    private double giveWeekEarnings(int week, int month, int year) {
        double earnings = 0;

        for(Transaction t:transactionListPresenter.getInteractor().getTransactions()){
            if(t.getType().equals(TransactionType.REGULARPAYMENT) || t.getType().equals(TransactionType.REGULARINCOME)){
                if(t.getDate().get(Calendar.YEAR) <= year && t.getEndDate().get(Calendar.YEAR)>=year){

                    int numberOfWeekTransactions = numberOfWeekTransactions(t,week,month,year);

                    if(t.getType().equals(TransactionType.REGULARINCOME)){
                        earnings = earnings + t.getAmount()*numberOfWeekTransactions;
                    }
                }
            }else{
                if(t.getDate().get(Calendar.MONTH) == month && t.getDate().get(Calendar.YEAR) == year && t.getDate().get(Calendar.WEEK_OF_MONTH) == week){
                    if(t.getType().equals(TransactionType.INDIVIDUALINCOME)){
                        earnings = earnings + t.getAmount();
                    }
                }
            }
        }

        return earnings;
    }

    private void setWeekConsumptionBarChart() {
        BarDataSet consumptionBarDataSet = new BarDataSet(dataValueWeekConsumption(),"Consumption");
        consumptionBarDataSet.setDrawValues(false);
        BarData consumptionBarData = new BarData();
        consumptionBarData.addDataSet(consumptionBarDataSet);
        barChartConsumption.setData(consumptionBarData);
        barChartConsumption.invalidate();
    }

    private ArrayList<BarEntry> dataValueWeekConsumption() {
        ArrayList<BarEntry> dataVals = new ArrayList<>();
        int year = TransactionListFragment.calendar.get(Calendar.YEAR);
        int month = TransactionListFragment.calendar.get(Calendar.MONTH);

        for(int week = 1; week<=TransactionListFragment.calendar.getActualMaximum(Calendar.WEEK_OF_MONTH); week++){
            dataVals.add(new BarEntry(week,(float)giveWeekConsumption(week,month,year)));
        }

        return dataVals;
    }

    private double giveWeekConsumption(int week, int month, int year) {
        double consumption = 0;

        for(Transaction t:transactionListPresenter.getInteractor().getTransactions()){
            if(t.getType().equals(TransactionType.REGULARPAYMENT) || t.getType().equals(TransactionType.REGULARINCOME)){
                if(t.getDate().get(Calendar.YEAR) <= year && t.getEndDate().get(Calendar.YEAR)>=year){

                    int numberOfWeekTransactions = numberOfWeekTransactions(t,week,month,year);

                    if(t.getType().equals(TransactionType.REGULARPAYMENT)){
                        consumption = consumption + t.getAmount()*numberOfWeekTransactions;
                    }
                }
            }else{
                if(t.getDate().get(Calendar.MONTH) == month && t.getDate().get(Calendar.YEAR) == year && t.getDate().get(Calendar.WEEK_OF_MONTH) == week){
                    if(t.getType().equals(TransactionType.INDIVIDUALPAYMENT) || t.getType().equals(TransactionType.PURCHASE)){
                        consumption = consumption + t.getAmount();
                    }
                }
            }
        }

        return consumption;
    }

    private int numberOfWeekTransactions(Transaction t, int week, int month, int year) {
        Calendar start = (Calendar) t.getDate().clone();
        Calendar end = (Calendar) t.getEndDate().clone();

        while(start.get(Calendar.YEAR)<year){
            start.add(Calendar.DAY_OF_YEAR,t.getTransactionInterval());
        }

        while(start.get(Calendar.MONTH)<month){
            start.add(Calendar.DAY_OF_YEAR,t.getTransactionInterval());
        }

        if(start.get(Calendar.YEAR)!=year){
            return 0;
        }

        while(start.get(Calendar.WEEK_OF_MONTH)<week){
            start.add(Calendar.DAY_OF_YEAR,t.getTransactionInterval());
        }

        if(start.get(Calendar.MONTH)!=month){
            return 0;
        }

        if(start.get(Calendar.WEEK_OF_MONTH) == week){
            int numberOfWeekTransactions = 0;

            while(start.get(Calendar.WEEK_OF_MONTH) == week && start.get(Calendar.MONTH) == month && start.get(Calendar.YEAR) == year &&
                start.compareTo(end)<0){
                numberOfWeekTransactions++;
                start.add(Calendar.DAY_OF_YEAR,t.getTransactionInterval());
            }

            return numberOfWeekTransactions;
        }

        return 0;
    }

    private void setDayTotalBarChart() {
        BarDataSet totalBarDataSet = new BarDataSet(dataValueDayTotal(),"Total");
        totalBarDataSet.setDrawValues(false);
        BarData totalBarData = new BarData();
        totalBarData.addDataSet(totalBarDataSet);
        barChartTotal.setData(totalBarData);
        barChartTotal.invalidate();
    }

    private ArrayList<BarEntry> dataValueDayTotal() {
        ArrayList<BarEntry> dataVals = new ArrayList<>();
        int year = TransactionListFragment.calendar.get(Calendar.YEAR);
        int month = TransactionListFragment.calendar.get(Calendar.MONTH);

        double total = 0;

        for(int day = 1; day<=TransactionListFragment.calendar.getActualMaximum(Calendar.DAY_OF_MONTH); day++){
            total = total + giveDayEarnings(day,month,year) - giveDayConsumption(day,month,year);
            dataVals.add(new BarEntry(day,(float)total));
        }

        return dataVals;
    }

    private void setDayEarningsBarChart() {
        BarDataSet earningsBarDataSet = new BarDataSet(dataValueDayEarnings(),"Earnings");
        earningsBarDataSet.setDrawValues(false);
        BarData earningsBarData = new BarData();
        earningsBarData.addDataSet(earningsBarDataSet);
        barChartEarnings.setData(earningsBarData);
        barChartEarnings.invalidate();
    }

    private ArrayList<BarEntry> dataValueDayEarnings() {
        ArrayList<BarEntry> dataVals = new ArrayList<>();
        int year = TransactionListFragment.calendar.get(Calendar.YEAR);
        int month = TransactionListFragment.calendar.get(Calendar.MONTH);

        for(int day = 1; day<=TransactionListFragment.calendar.getActualMaximum(Calendar.DAY_OF_MONTH); day++){
            dataVals.add(new BarEntry(day,(float)giveDayEarnings(day,month,year)));
        }

        return dataVals;
    }

    private double giveDayEarnings(int day, int month, int year) {
        double earnings = 0;

        for(Transaction t:transactionListPresenter.getInteractor().getTransactions()){
            if(t.getType().equals(TransactionType.REGULARPAYMENT) || t.getType().equals(TransactionType.REGULARINCOME)){
                if(t.getDate().get(Calendar.YEAR) <= year && t.getEndDate().get(Calendar.YEAR)>=year){

                    if(wasTransactionOnDay(t,day,month,year)){
                        if(t.getType().equals(TransactionType.REGULARINCOME)){
                            earnings = earnings + t.getAmount();
                        }
                    }

                }
            }else{
                if(t.getDate().get(Calendar.MONTH) == month && t.getDate().get(Calendar.YEAR) == year && t.getDate().get(Calendar.DAY_OF_MONTH) == day){
                    if(t.getType().equals(TransactionType.INDIVIDUALINCOME)){
                        earnings = earnings + t.getAmount();
                    }
                }
            }
        }

        return earnings;
    }

    private void setDayConsumptionBarChart() {
        BarDataSet consumptionBarDataSet = new BarDataSet(dataValueDayConsumption(),"Consumption");
        consumptionBarDataSet.setDrawValues(false);
        BarData consumptionBarData = new BarData();
        consumptionBarData.addDataSet(consumptionBarDataSet);
        barChartConsumption.setData(consumptionBarData);
        barChartConsumption.invalidate();
    }

    private ArrayList<BarEntry> dataValueDayConsumption() {
        ArrayList<BarEntry> dataVals = new ArrayList<>();
        int year = TransactionListFragment.calendar.get(Calendar.YEAR);
        int month = TransactionListFragment.calendar.get(Calendar.MONTH);

        for(int day = 1; day<=TransactionListFragment.calendar.getActualMaximum(Calendar.DAY_OF_MONTH); day++){
            dataVals.add(new BarEntry(day,(float)giveDayConsumption(day,month,year)));
        }

        return dataVals;
    }

    private double giveDayConsumption(int day, int month, int year) {
        double consumption = 0;

        for(Transaction t:transactionListPresenter.getInteractor().getTransactions()){
            if(t.getType().equals(TransactionType.REGULARPAYMENT) || t.getType().equals(TransactionType.REGULARINCOME)){
                if(t.getDate().get(Calendar.YEAR) <= year && t.getEndDate().get(Calendar.YEAR)>=year){

                    if(wasTransactionOnDay(t,day,month,year)){
                        if(t.getType().equals(TransactionType.REGULARPAYMENT)){
                            consumption = consumption + t.getAmount();
                        }
                    }

                }
            }else{
                if(t.getDate().get(Calendar.MONTH) == month && t.getDate().get(Calendar.YEAR) == year && t.getDate().get(Calendar.DAY_OF_MONTH) == day){
                    if(t.getType().equals(TransactionType.INDIVIDUALPAYMENT) || t.getType().equals(TransactionType.PURCHASE)){
                        consumption = consumption + t.getAmount();
                    }
                }
            }
        }

        return consumption;
    }

    private boolean wasTransactionOnDay(Transaction t, int day, int month, int year) {
        Calendar start = (Calendar) t.getDate().clone();
        Calendar end = (Calendar) t.getEndDate().clone();

        while(start.get(Calendar.YEAR) < year){
            start.add(Calendar.DAY_OF_YEAR,t.getTransactionInterval());
        }

        while(start.get(Calendar.MONTH) < month){
            start.add(Calendar.DAY_OF_YEAR,t.getTransactionInterval());
        }

        if(start.get(Calendar.YEAR) != year){
            return false;
        }

        while(start.get(Calendar.DAY_OF_MONTH)<day && start.get(Calendar.MONTH)== month && start.compareTo(end) < 0){
            start.add(Calendar.DAY_OF_MONTH,t.getTransactionInterval());
        }

        if(start.get(Calendar.DAY_OF_MONTH)==day){
            return true;
        }
        return false;
    }

    private void setMonthTotalBarChart() {
        BarDataSet totalBarDataSet = new BarDataSet(dataValueMonthTotal(),"Total");
        totalBarDataSet.setDrawValues(false);
        BarData totalBarData = new BarData();
        totalBarData.addDataSet(totalBarDataSet);
        barChartTotal.setData(totalBarData);
        barChartTotal.invalidate();
    }

    private ArrayList<BarEntry> dataValueMonthTotal() {
        ArrayList<BarEntry> dataVals = new ArrayList<>();
        int year = TransactionListFragment.calendar.get(Calendar.YEAR);

        double total = 0;

        for(int month=0;month<12;month++){
            total = total + giveMonthEarnings(month,year) - giveMonthConsumption(month,year);
            dataVals.add(new BarEntry(month+1,(float)total));
        }

        return dataVals;
    }

    private void setMonthEarningsBarChart() {
        BarDataSet earningsBarDataSet = new BarDataSet(dataValueMonthEarnings(),"Earnings");
        earningsBarDataSet.setDrawValues(false);
        BarData earningsBarData = new BarData();
        earningsBarData.addDataSet(earningsBarDataSet);
        barChartEarnings.setData(earningsBarData);
        barChartEarnings.invalidate();
    }

    private ArrayList<BarEntry> dataValueMonthEarnings() {
        ArrayList<BarEntry> dataVals = new ArrayList<>();
        int year = TransactionListFragment.calendar.get(Calendar.YEAR);

        for(int month=0;month<12;month++){
            dataVals.add(new BarEntry(month+1,(float)giveMonthEarnings(month,year)));
        }

        return dataVals;
    }

    private double giveMonthEarnings(int month, int year) {
        double earnings = 0;

        for(Transaction t:transactionListPresenter.getInteractor().getTransactions()){
            if(t.getType().equals(TransactionType.REGULARPAYMENT) || t.getType().equals(TransactionType.REGULARINCOME)){
                if(t.getDate().get(Calendar.YEAR) <= year && t.getEndDate().get(Calendar.YEAR)>=year){

                    int numberOfMonthTransactions = giveNumbeOfMonthTransactions(t,month,year);

                    if(t.getType().equals(TransactionType.REGULARINCOME)){
                        earnings = earnings + t.getAmount()*numberOfMonthTransactions;
                    }
                }
            }else{
                if(t.getDate().get(Calendar.MONTH) == month && t.getDate().get(Calendar.YEAR) == year){
                    if(t.getType().equals(TransactionType.INDIVIDUALINCOME)){
                        earnings = earnings + t.getAmount();
                    }
                }
            }
        }

        return earnings;
    }

    private void setMonthConsumptionBarChart() {
        BarDataSet consumptionBarDataSet = new BarDataSet(dataValueMonthConsumption(),"Consumption");
        consumptionBarDataSet.setDrawValues(false);
        BarData consumptionBarData = new BarData();
        consumptionBarData.addDataSet(consumptionBarDataSet);
        barChartConsumption.setData(consumptionBarData);
        barChartConsumption.invalidate();
    }

    private ArrayList<BarEntry> dataValueMonthConsumption(){

        ArrayList<BarEntry> dataVals = new ArrayList<>();
        int year = TransactionListFragment.calendar.get(Calendar.YEAR);

        for(int month=0;month<12;month++){
            dataVals.add(new BarEntry(month+1,(float)giveMonthConsumption(month,year)));
        }

        return dataVals;
    }

    private double giveMonthConsumption(int month, int year) {
        double consumption = 0;

        for(Transaction t:transactionListPresenter.getInteractor().getTransactions()){
            if(t.getType().equals(TransactionType.REGULARPAYMENT) || t.getType().equals(TransactionType.REGULARINCOME)){
                if(t.getDate().get(Calendar.YEAR) <= year && t.getEndDate().get(Calendar.YEAR)>=year){

                    int numberOfMonthTransactions = giveNumbeOfMonthTransactions(t,month,year);

                    if(t.getType().equals(TransactionType.REGULARPAYMENT)){
                        consumption = consumption + t.getAmount()*numberOfMonthTransactions;
                    }
                }
            }else{
                if(t.getDate().get(Calendar.MONTH) == month && t.getDate().get(Calendar.YEAR) == year){
                    if(t.getType().equals(TransactionType.INDIVIDUALPAYMENT) || t.getType().equals(TransactionType.PURCHASE)){
                        consumption = consumption + t.getAmount();
                    }
                }
            }
        }

        return consumption;
    }

    private int giveNumbeOfMonthTransactions(Transaction t, int month, int year) {
        if(t.getType().equals(TransactionType.REGULARPAYMENT) || t.getType().equals(TransactionType.REGULARINCOME)){
            Calendar start = (Calendar) t.getDate().clone();
            Calendar end = (Calendar) t.getEndDate().clone();

            while(start.get(Calendar.YEAR) < year){
                start.add(Calendar.DAY_OF_YEAR,t.getTransactionInterval());
            }

            while(start.get(Calendar.MONTH) < month){
                start.add(Calendar.DAY_OF_YEAR,t.getTransactionInterval());
            }

            int numberOfMonthTransactions = 0;
            while(start.get(Calendar.MONTH) == month && start.get(Calendar.YEAR) == year && start.compareTo(end)<0){
                start.add(Calendar.DAY_OF_YEAR,t.getTransactionInterval());
                numberOfMonthTransactions++;
            }

            return numberOfMonthTransactions;
        }

        return 1;
    }
}
