package ba.unsa.etf.rma.rma20mujanovicemir04;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements TransactionListFragment.OnItemClick,
        TransactionListFragment.OnAddBtnClick, TransactionDetailFragment.OnTransactionDeleteOrAdd, TransactionDetailFragment.OnTransactionEdit,
        TransactionListFragment.OnMonthChange, TransactionListFragment.OnLeftSwipe, TransactionListFragment.OnRightSwipe,
        TransactionListInteractor.OnTransactionsDone, AccountInteractor.OnAccountUpdateDone {

    private ConnectivityBroadcastReceiver receiver = new ConnectivityBroadcastReceiver(this);
    private IntentFilter filter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");


    private boolean landscape = false;
    private boolean transactionSelected = false;
    private Transaction selectedTransaction;

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(receiver, filter);
    }

    @Override
    public void onPause() {
        unregisterReceiver(receiver);
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        updateTransactionTypes();

        FragmentManager fragmentManager = getSupportFragmentManager();
        ScrollView details = findViewById(R.id.transaction_detail);
        if (details != null) {
            landscape = true;
            TransactionDetailFragment detailFragment = (TransactionDetailFragment) fragmentManager.findFragmentById(R.id.transaction_detail);
            if (detailFragment==null) {
                detailFragment = new TransactionDetailFragment();
                fragmentManager.beginTransaction().replace(R.id.transaction_detail,detailFragment).commit();
            }else{
                //Ovaj dio je potreban da kada iz landscape moda, prebacimo u portrait zatim opet u landscape
                //DetailFragment bude prazan
                detailFragment = new TransactionDetailFragment();
                fragmentManager.beginTransaction().replace(R.id.transaction_detail,detailFragment).commit();
            }
        } else {
            landscape = false;
        }

        Fragment listFragment = fragmentManager.findFragmentByTag("list");
        if (listFragment==null){
            listFragment = new TransactionListFragment();
            fragmentManager.beginTransaction().replace(R.id.transaction_list,listFragment,"list").commit();
        }else{
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

    }

    @Override
    public void onItemClicked(Transaction transaction) {
        Bundle arguments = new Bundle();

        TransactionListPresenter transactionListPresenter = new TransactionListPresenter(this,new TransactionListInteractor(this));

        /*
        ArrayList<Transaction> transactions = transactionListPresenter.getInteractor().getTransactions();

        int k;
        for(k=0;k<transactions.size();k++){
            if(transaction.equals(transactions.get(k))){
                break;
            }
        }
        */

        arguments.putParcelable("transaction",transaction);
        arguments.putInt("transactionNumber",transaction.getId());
        TransactionDetailFragment detailFragment = new TransactionDetailFragment();
        detailFragment.setArguments(arguments);

        if (landscape){
            if(transactionSelected && selectedTransaction!=null && selectedTransaction.equals(transaction)){
                getSupportFragmentManager().beginTransaction().replace(R.id.transaction_detail, new TransactionDetailFragment()).commit();
                transactionSelected = false;
                updateSelector();
            }else{
                getSupportFragmentManager().beginTransaction().replace(R.id.transaction_detail, detailFragment).commit();
                transactionSelected = true;
                updateSelector();
            }
        }
        else{
            getSupportFragmentManager().beginTransaction().replace(R.id.transaction_list,detailFragment).addToBackStack(null).commit();
        }

        selectedTransaction = transaction;
        updateAddButton();
    }

    @Override
    public void onAddBtnClicked(Calendar c) {
        Bundle arguments = new Bundle();
        Transaction transaction = new Transaction(c,0,"",TransactionType.INDIVIDUALINCOME,"",0,(Calendar)c.clone());
        transaction.setId(-1);
        transaction.setMode(TransactionDBOpenHelper.TRANSACTION_ADDED);

        arguments.putParcelable("transaction",transaction);
        arguments.putInt("transactionNumber",-1);
        TransactionDetailFragment detailFragment = new TransactionDetailFragment();
        detailFragment.setArguments(arguments);
        if (landscape){
            getSupportFragmentManager().beginTransaction().replace(R.id.transaction_detail, detailFragment).commit();
        }
        else{
            getSupportFragmentManager().beginTransaction().replace(R.id.transaction_list,detailFragment).addToBackStack(null).commit();
        }
    }

    @Override
    public void onTransactionDeletedOrAdded(boolean deleted) {
        if(landscape){
            TransactionListFragment transactionListFragment = (TransactionListFragment) getSupportFragmentManager().findFragmentById(R.id.transaction_list);
            if(transactionListFragment!=null){
                transactionListFragment.notifyDataSetChanged();
                transactionListFragment.refresh();
            }

            if(!deleted || ConnectivityBroadcastReceiver.isConnected()){
                TransactionDetailFragment emptyFragment = new TransactionDetailFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.transaction_detail,emptyFragment).commit();
            }
        }
    }

    @Override
    public void onTransactionEdited() {
        if(landscape){
            TransactionListFragment transactionListFragment = (TransactionListFragment) getSupportFragmentManager().findFragmentById(R.id.transaction_list);
            if(transactionListFragment!=null){
                transactionListFragment.notifyDataSetChanged();
                transactionListFragment.refresh();
            }
        }
    }

    private void updateAddButton(){
        if(landscape){
            TransactionListFragment transactionListFragment = (TransactionListFragment) getSupportFragmentManager().findFragmentById(R.id.transaction_list);
            if(transactionSelected){
                transactionListFragment.disableAddButton();
            }else{
                transactionListFragment.enableAddButton();
            }
        }
    }

    private void updateSelector(){
        if(landscape){
            TransactionListFragment transactionListFragment = (TransactionListFragment) getSupportFragmentManager().findFragmentById(R.id.transaction_list);
            if(transactionSelected){
                transactionListFragment.enableSelector();
            }else{
                transactionListFragment.disableSelector();
            }
        }
    }

    @Override
    public void onMonthChanged() {
        if(landscape){
            TransactionDetailFragment emptyFragment = new TransactionDetailFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.transaction_detail,emptyFragment).commit();
            transactionSelected = false;
            updateSelector();
        }
    }

    @Override
    public void onLeftSwiped(Fragment f) {
        if(!landscape){
            if( f instanceof TransactionListFragment){
                BudgetFragment budgetFragment = new BudgetFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.transaction_list,budgetFragment).commit();
            }else if( f instanceof BudgetFragment){
                GraphsFragment graphsFragment = new GraphsFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.transaction_list,graphsFragment).commit();
            }else if( f instanceof GraphsFragment){
                TransactionListFragment listFragment = new TransactionListFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.transaction_list,listFragment).commit();
            }
        }
    }


    @Override
    public void onRightSwiped(Fragment f) {
        if(!landscape){
            if(f instanceof BudgetFragment){
                TransactionListFragment listFragment = new TransactionListFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.transaction_list,listFragment).commit();
            }else if(f instanceof TransactionListFragment){
                GraphsFragment graphsFragment = new GraphsFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.transaction_list,graphsFragment).commit();
            }else if(f instanceof GraphsFragment){
                BudgetFragment budgetFragment = new BudgetFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.transaction_list,budgetFragment).commit();
            }
        }
    }


    @Override
    public void onDone() {
        Fragment listFragment = getSupportFragmentManager().findFragmentById(R.id.transaction_list);

        if(listFragment instanceof TransactionListFragment){
            TransactionListFragment transactionListFragment = (TransactionListFragment) listFragment;
            transactionListFragment.refreshList();
        }else if(listFragment instanceof GraphsFragment){
            GraphsFragment graphsFragment = (GraphsFragment) listFragment;
            graphsFragment.updateGraphs();
        }
    }

    @Override
    public void onAccountDone() {
        Fragment listFragment = getSupportFragmentManager().findFragmentById(R.id.transaction_list);
        if(listFragment instanceof TransactionListFragment){
            ((TransactionListFragment) listFragment).refreshBudget();
        }else if(listFragment instanceof BudgetFragment){
            ((BudgetFragment) listFragment).refreshBudget();
        }
    }

    @Override
    public void continueUpdate(double budget, double totalLimit, double monthLimit) {
        //OVO SE MORA IMPLEMENTOVATI DA NE BI IZBACIVALO GRESKU
    }

    private void updateTransactionTypes() {
        if(ConnectivityBroadcastReceiver.isConnected()){
            new TransactionTypeInteractor().execute();
        }else{
            TransactionType.transactionTypeHashMap = new HashMap<>();
            TransactionType.transactionTypeHashMap.put(1,TransactionType.REGULARPAYMENT);
            TransactionType.transactionTypeHashMap.put(2,TransactionType.REGULARINCOME);
            TransactionType.transactionTypeHashMap.put(3,TransactionType.PURCHASE);
            TransactionType.transactionTypeHashMap.put(4,TransactionType.INDIVIDUALINCOME);
            TransactionType.transactionTypeHashMap.put(5,TransactionType.INDIVIDUALPAYMENT);
        }
    }
}

