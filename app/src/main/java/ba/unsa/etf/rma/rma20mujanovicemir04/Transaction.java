package ba.unsa.etf.rma.rma20mujanovicemir04;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.core.os.ParcelableCompatCreatorCallbacks;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class Transaction implements Parcelable {
    private int internalId;
    private int id;
    private Calendar date;
    private double amount;
    private String title;
    private TransactionType type;
    private String itemDescription;
    private int transactionInterval;
    private Calendar endDate;
    private int mode;

    public Transaction(Parcel in){
        internalId = in.readInt();
        id = in.readInt();

        date = Calendar.getInstance();
        date.setTimeInMillis(in.readLong());

        amount = in.readDouble();
        title = in.readString();
        type = TransactionType.valueOf(in.readString());
        itemDescription = in.readString();
        transactionInterval = in.readInt();
        Long l = in.readLong();
        if(l==-1){
            endDate = null;
        }else{
            endDate = Calendar.getInstance();
            endDate.setTimeInMillis(l);
        }
        mode = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(internalId);
        dest.writeInt(id);
        dest.writeLong(date.getTimeInMillis());
        dest.writeDouble(amount);
        dest.writeString(title);
        dest.writeString(type.name());
        dest.writeString(itemDescription);
        dest.writeInt(transactionInterval);
        if(endDate!=null){
            dest.writeLong(endDate.getTimeInMillis());
        }else{
            dest.writeLong(-1);
        }
        dest.writeInt(mode);
    }

    public static final Creator<Transaction> CREATOR = new Creator<Transaction>() {
        @Override
        public Transaction createFromParcel(Parcel in) {
            return new Transaction(in);
        }

        @Override
        public Transaction[] newArray(int size) {
            return new Transaction[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    public Transaction(Calendar date, double amount, String title, TransactionType type, String itemDescription, int transactionInterval, Calendar endDate) {
        this.internalId = 0;
        this.date = date;
        this.amount = amount;
        this.title = title;
        this.type = type;
        this.itemDescription = itemDescription;
        this.transactionInterval = transactionInterval;
        this.endDate = endDate;
        this.mode = -1;
    }

    public int getInternalId() {
        return internalId;
    }

    public void setInternalId(int internalId) {
        this.internalId = internalId;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public TransactionType getType() {
        return type;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public int getTransactionInterval() {
        return transactionInterval;
    }

    public void setTransactionInterval(int transactionInterval) {
        this.transactionInterval = transactionInterval;
    }

    public Calendar getEndDate() {
        return endDate;
    }

    public void setEndDate(Calendar endDate) {
        this.endDate = endDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return id==that.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, amount, title);
    }


}
