package ba.unsa.etf.rma.rma20mujanovicemir04;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class TransactionDBOpenHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "TransactionDataBase.db";
    public static final int DATABASE_VERSION = 1;

    public TransactionDBOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version){
        super(context, name, factory, version);
    }

    public TransactionDBOpenHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static final String TRANSACTION_TABLE = "transactions";
    public static final String TRANSACTION_INTERNAL_ID = "_id";
    public static final String TRANSACTION_ID = "id";
    public static final String TRANSACTION_DATE = "date";
    public static final String TRANSACTION_AMOUNT = "amount";
    public static final String TRANSACTION_TITLE = "title";
    public static final String TRANSACTION_TYPE = "type";
    public static final String TRANSACTION_DESCRIPTION = "descrtiption";
    public static final String TRANSACTION_INTERVAL = "interval";
    public static final String TRANSACTION_END_DATE = "endDate";
    public static final String TRANSACTION_MODE = "mode";

    public static final int TRANSACTION_ADDED = 0;
    public static final int TRANSACTION_EDITED = 1;
    public static final int TRANSACTION_DELETED = 2;

    public static final String TRANSACTION_TABLE_CREATE =
            "CREATE TABLE IF NOT EXISTS " + TRANSACTION_TABLE + " (" + TRANSACTION_INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + TRANSACTION_ID + " INTEGER, "
                + TRANSACTION_DATE + " TEXT, "
                + TRANSACTION_AMOUNT + " REAL NOT NULL, "
                + TRANSACTION_TITLE + " TEXT, "
                + TRANSACTION_TYPE + " INTEGER, "
                + TRANSACTION_DESCRIPTION + " TEXT, "
                + TRANSACTION_INTERVAL + " INTEGER, "
                + TRANSACTION_END_DATE + " TEXT, "
                + TRANSACTION_MODE + " INTEGER);";

    private static final String TRANSACTION_DROP = "DROP TABLE IF EXISTS " + TRANSACTION_TABLE;

    public static final String ACCOUNT_TABLE = "account";
    public static final String ACCOUNT_ID = "_id";
    public static final String ACCOUNT_BUDGET = "budget";
    public static final String ACCOUNT_TOTAL_LIMIT = "totalLimit";
    public static final String ACCOUNT_MONTH_LIMIT = "monthLimit";

    private static final String ACCOUNT_TABLE_CREATE =
            "CREATE TABLE IF NOT EXISTS " + ACCOUNT_TABLE + " (" + ACCOUNT_ID + " INTEGER PRIMARY KEY, "
                + ACCOUNT_BUDGET + " REAL, "
                + ACCOUNT_TOTAL_LIMIT + " REAL, "
                + ACCOUNT_MONTH_LIMIT + " REAL);";

    private static final String ACCOUNT_DROP = "DROP TABLE IF EXISTS " + ACCOUNT_TABLE;

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TRANSACTION_TABLE_CREATE);
        db.execSQL(ACCOUNT_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(TRANSACTION_DROP);
        db.execSQL(ACCOUNT_DROP);
        onCreate(db);
    }
}
