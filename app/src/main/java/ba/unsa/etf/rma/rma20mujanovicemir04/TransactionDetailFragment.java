package ba.unsa.etf.rma.rma20mujanovicemir04;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;

public class TransactionDetailFragment extends Fragment {
    private ImageView icon;
    private EditText editTitle;
    private EditText editAmount;
    private TextView editDate;
    private Spinner spinnerType;
    private EditText editDescription;
    private EditText editInterval;
    private TextView editEndDate;
    private Button saveBtn;
    private Button deleteBtn;
    private TextView textMode;

    public static boolean dataFromDB = true;

    private double monthExpense;

    private TransactionListPresenter transactionListPresenter;
    private AccountPresenter accountPresenter;

    private DatePickerDialog.OnDateSetListener dateSetListener1;
    private DatePickerDialog.OnDateSetListener dateSetListener2;

    private boolean firstTime = true;

    private Transaction t;
    private int transactionNumber;
    private double oldBudget;
    private double oldAmount;
    private TransactionType oldType;
    private TransactionType type;
    private boolean correctData = true;

    public interface OnTransactionDeleteOrAdd{
        void onTransactionDeletedOrAdded(boolean deleted);
    }

    public interface OnTransactionEdit{
        void onTransactionEdited();
    }

    private OnTransactionDeleteOrAdd otdoa;
    private OnTransactionEdit ote;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.transaction_detail,container,false);

        icon = (ImageView)view.findViewById(R.id.icon);
        editTitle = (EditText)view.findViewById(R.id.editTitle);
        editAmount = (EditText)view.findViewById(R.id.editAmount);
        editDate = (TextView)view.findViewById(R.id.editDate);
        spinnerType = (Spinner)view.findViewById(R.id.spinnerType);
        editDescription = (EditText)view.findViewById(R.id.editDescription);
        editInterval = (EditText)view.findViewById(R.id.editInterval);
        editEndDate = (TextView)view.findViewById(R.id.editEndDate);
        saveBtn = (Button)view.findViewById(R.id.saveBtn);
        deleteBtn = (Button)view.findViewById(R.id.deleteBtn);
        textMode = (TextView) view.findViewById(R.id.textMode);

        if(getArguments()!=null){
            transactionNumber = getArguments().getInt("transactionNumber");
            t = getArguments().getParcelable("transaction");
        }else{
            return view;
        }

        if(transactionNumber==-1){
            deleteBtn.setEnabled(false);
        }

        updateOfflineTextView();

        transactionListPresenter = new TransactionListPresenter(getContext(),new TransactionListInteractor(getContext()));
        accountPresenter = new AccountPresenter(getContext(),new AccountInteractor(getContext()));

        otdoa = (OnTransactionDeleteOrAdd) getActivity();
        ote = (OnTransactionEdit) getActivity();

        setFilterSpinner();

        if(t.getType().equals(TransactionType.PURCHASE)){
            icon.setImageResource(R.drawable.purchase);
            spinnerType.setSelection(0);
        }else if(t.getType().equals(TransactionType.INDIVIDUALINCOME)){
            icon.setImageResource(R.drawable.individualincome);
            spinnerType.setSelection(1);
        }else if(t.getType().equals(TransactionType.INDIVIDUALPAYMENT)){
            icon.setImageResource(R.drawable.individualpayment);
            spinnerType.setSelection(2);
        }else if(t.getType().equals(TransactionType.REGULARINCOME)){
            icon.setImageResource(R.drawable.regularincome);
            spinnerType.setSelection(3);
        }else{
            icon.setImageResource(R.drawable.regularpayment);
            spinnerType.setSelection(4);
        }


        editTitle.setText(t.getTitle());
        editAmount.setText(Double.toString(t.getAmount()));
        System.out.println(t.getDate());
        editDate.setText(t.getDate().get(Calendar.DAY_OF_MONTH) + "." + (t.getDate().get(Calendar.MONTH)+1) + "." + t.getDate().get(Calendar.YEAR));

        editDescription.setText(t.getItemDescription());

        if(t.getType().equals(TransactionType.REGULARINCOME) ||
                t.getType().equals(TransactionType.REGULARPAYMENT)){
            editInterval.setText(Integer.toString(t.getTransactionInterval()));
            editEndDate.setText(t.getEndDate().get(Calendar.DAY_OF_MONTH) + "." + (t.getEndDate().get(Calendar.MONTH)+1) + "." + t.getEndDate().get(Calendar.YEAR));
        }else{
            editEndDate.setText(t.getDate().get(Calendar.DAY_OF_MONTH) + "." + (t.getDate().get(Calendar.MONTH)+1) + "." + t.getDate().get(Calendar.YEAR));
        }

        editTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                editTitle.setBackgroundColor(getResources().getColor(R.color.colorGreen));
            }
        });

        editAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                editAmount.setBackgroundColor(getResources().getColor(R.color.colorGreen));
            }
        });

        editDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                editDescription.setBackgroundColor(getResources().getColor(R.color.colorGreen));
            }
        });

        editInterval.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                editInterval.setBackgroundColor(getResources().getColor(R.color.colorGreen));
            }
        });

        editDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                editDate.setBackgroundColor(getResources().getColor(R.color.colorGreen));
            }
        });

        editEndDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                editEndDate.setBackgroundColor(getResources().getColor(R.color.colorGreen));
            }
        });

        editDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = dateFromString(editDate.getText().toString());

                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        android.R.style.Theme_Dialog,
                        dateSetListener1,
                        c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH)
                );

                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();
            }
        });


        editEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = dateFromString(editEndDate.getText().toString());

                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        android.R.style.Theme_Dialog,
                        dateSetListener2,
                        c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH)
                );

                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();
            }
        });

        dateSetListener1 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                editDate.setText(dayOfMonth + "." + (month+1) + "." + year);
            }
        };

        dateSetListener2 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                editEndDate.setText(dayOfMonth + "." + (month+1) + "." + year);
            }
        };

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if((spinnerType.getSelectedItem()).equals("Purchase")){
                    type = TransactionType.PURCHASE;
                }else if((spinnerType.getSelectedItem()).equals("Individual income")){
                    type = TransactionType.INDIVIDUALINCOME;
                }else if((spinnerType.getSelectedItem()).equals("Individual payment")){
                    type = TransactionType.INDIVIDUALPAYMENT;
                }else if((spinnerType.getSelectedItem()).equals("Regular income")){
                    type = TransactionType.REGULARINCOME;
                }else{
                    type = TransactionType.REGULARPAYMENT;
                }

                correctData = true;
                validateData();

                if(correctData){
                    oldBudget = accountPresenter.getInteractor().getAccount().getBudget();

                    monthExpense = giveMonthExpense(dateFromString(editDate.getText().toString()).get(Calendar.MONTH));

                    updateBudget();

                    if(oldBudget<accountPresenter.getInteractor().getAccount().getTotalLimit()){
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setTitle("You are breaching your total limit");
                        builder.setMessage("Are you sure you want to continue?");
                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                addTransaction();

                                if(transactionNumber==-1){
                                    otdoa.onTransactionDeletedOrAdded(false);
                                    getFragmentManager().popBackStack();
                                }else{
                                    ote.onTransactionEdited();
                                }
                            }
                        });

                        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });

                        builder.create().show();
                    }else{
                        if(monthExpense > accountPresenter.getInteractor().getAccount().getMonthLimit()){
                            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                            builder.setTitle("You have breached the month limit on your account");
                            builder.setMessage("Are you sure you want to continue?");
                            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    addTransaction();

                                    if(transactionNumber==-1){
                                        otdoa.onTransactionDeletedOrAdded(false);
                                        getFragmentManager().popBackStack();
                                    }else{
                                        ote.onTransactionEdited();
                                    }
                                }
                            });

                            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });

                            builder.create().show();
                        }else{
                            addTransaction();

                            if(transactionNumber==-1){
                                otdoa.onTransactionDeletedOrAdded(false);
                                getFragmentManager().popBackStack();
                            }else{
                                ote.onTransactionEdited();
                            }
                        }
                    }
                }
            }
        });

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(deleteBtn.getText().equals(getString(R.string.delete))){
                    AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                    alert.setTitle("Delete a transaction");
                    alert.setMessage("Are you sure?");

                    alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            oldBudget = accountPresenter.getInteractor().getAccount().getBudget();
                            updateBudgetForDelete();

                            transactionListPresenter.getInteractor().getTransactions().remove(t);

                            t.setInternalId(transactionListPresenter.getInteractor().getInternalIdFromTransaction(t));
                            transactionListPresenter.deleteTransaction(t);

                            accountPresenter.getInteractor().getAccount().setBudget(oldBudget);
                            accountPresenter.updateAccount(oldBudget, accountPresenter.getInteractor().getAccount().getTotalLimit(),
                                    accountPresenter.getInteractor().getAccount().getMonthLimit());

                            otdoa.onTransactionDeletedOrAdded(true);

                            if(ConnectivityBroadcastReceiver.isConnected()){
                                getFragmentManager().popBackStack();
                            }else{
                                deleteBtn.setText(R.string.undo);
                                textMode.setText("Offline\ndelete");
                            }
                        }
                    });

                    alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

                    alert.create().show();
                }else {
                    t.setInternalId(transactionListPresenter.getInteractor().getInternalIdFromTransaction(t));
                    transactionListPresenter.undoDelete(t);

                    if(dataFromDB && t.getId()!=0){
                        transactionListPresenter.getInteractor().getTransactions().remove(t);
                    }else{

                        //Updatovanje moda u listi
                        for(Transaction tr : transactionListPresenter.getInteractor().getTransactions()){
                            if(tr.getInternalId() == t.getInternalId()){

                                int index = transactionListPresenter.getInteractor().getTransactions().indexOf(t);

                                if(t.getId()==0){

                                    transactionListPresenter.getInteractor().getTransactions().get(index).setMode(TransactionDBOpenHelper.TRANSACTION_ADDED);
                                }else{
                                    transactionListPresenter.getInteractor().getTransactions().get(index).setMode(TransactionDBOpenHelper.TRANSACTION_EDITED);
                                }
                                break;
                            }
                        }
                    }

                    if(t.getId()==0){
                        textMode.setText("Offline\nadd");
                    }else{
                        textMode.setText("Offline\nedit");
                    }

                    //Update account
                    oldBudget = accountPresenter.getInteractor().getAccount().getBudget();
                    updateBudgetForUndo();

                    accountPresenter.getInteractor().getAccount().setBudget(oldBudget);
                    accountPresenter.updateAccount(oldBudget, accountPresenter.getInteractor().getAccount().getTotalLimit(),
                            accountPresenter.getInteractor().getAccount().getMonthLimit());

                    deleteBtn.setText(R.string.delete);
                }
            }
        });

        return view;
    }


    public void clearOfflineTextView(){
        textMode.setText("");
        deleteBtn.setText(R.string.delete);
    }

    public void updateOfflineTextView(){
        if(t!=null){
            if(!ConnectivityBroadcastReceiver.isConnected()){
                if(t.getMode()==-1 || t.getMode()==1){
                    textMode.setText("Offline\nedit");
                }else if(t.getMode()==0){
                    textMode.setText("Offline\nadd");
                }else if(t.getMode()==2){
                    textMode.setText("Offline\ndelete");
                    deleteBtn.setText(R.string.undo);
                }
            }
        }
    }

    private void updateBudget() {
        if(transactionNumber>-1){
            oldAmount = t.getAmount();
            oldType = t.getType();

            int numberOfTransactions = getTotalNumberOfTransactions(t, true);

            if(oldType.equals(TransactionType.INDIVIDUALINCOME) || oldType.equals(TransactionType.REGULARINCOME)){
                oldBudget = oldBudget - oldAmount*numberOfTransactions;
                monthExpense = monthExpense + oldAmount*numberOfTransactions;
            }else{
                oldBudget = oldBudget + oldAmount*numberOfTransactions;
                monthExpense = monthExpense - oldAmount*numberOfTransactions;
            }
        }

        int numberOfTransactions = getTotalNumberOfTransactions(readTransaction(), false);

        if(type.equals(TransactionType.INDIVIDUALINCOME) || type.equals(TransactionType.REGULARINCOME)){
            oldBudget = oldBudget + Double.parseDouble(editAmount.getText().toString()) * numberOfTransactions;
            monthExpense = monthExpense - Double.parseDouble(editAmount.getText().toString()) * numberOfTransactions;
        }else{
            oldBudget = oldBudget - Double.parseDouble(editAmount.getText().toString()) * numberOfTransactions;
            monthExpense = monthExpense + Double.parseDouble(editAmount.getText().toString()) * numberOfTransactions;
        }
    }

    private void updateBudgetForUndo() {
        if(transactionNumber>-1){
            oldAmount = t.getAmount();
            oldType = t.getType();

            int numberOfTransactions = getTotalNumberOfTransactions(t, true);

            if(oldType.equals(TransactionType.INDIVIDUALINCOME) || oldType.equals(TransactionType.REGULARINCOME)){
                oldBudget = oldBudget + oldAmount*numberOfTransactions;
                monthExpense = monthExpense - oldAmount*numberOfTransactions;
            }else{
                oldBudget = oldBudget - oldAmount*numberOfTransactions;
                monthExpense = monthExpense + oldAmount*numberOfTransactions;
            }
        }
    }

    private void updateBudgetForDelete(){
        if(transactionNumber>-1){
            oldAmount = t.getAmount();
            oldType = t.getType();

            int numberOfTransactions = getTotalNumberOfTransactions(t, true);

            if(oldType.equals(TransactionType.INDIVIDUALINCOME) || oldType.equals(TransactionType.REGULARINCOME)){
                oldBudget = oldBudget - oldAmount*numberOfTransactions;
                monthExpense = monthExpense + oldAmount*numberOfTransactions;
            }else{
                oldBudget = oldBudget + oldAmount*numberOfTransactions;
                monthExpense = monthExpense - oldAmount*numberOfTransactions;
            }
        }
    }

    private double giveMonthExpense(int month) {
        double expense = 0;
        for(Transaction t:transactionListPresenter.getInteractor().getTransactions()){
            if(t.getType().equals(TransactionType.REGULARPAYMENT) || t.getType().equals(TransactionType.REGULARINCOME)){
                if(t.getDate().get(Calendar.MONTH) <= month && t.getEndDate().get(Calendar.MONTH)>=month){

                    int numberOfMonthTransactions = giveNumbeOfMonthTransactions(t,month);

                    if(t.getType().equals(TransactionType.REGULARINCOME)){
                        expense = expense - t.getAmount()*numberOfMonthTransactions;
                    }else{
                        expense = expense + t.getAmount()*numberOfMonthTransactions;
                    }
                }
            }else{
                if(t.getDate().get(Calendar.MONTH) == month){
                    if(t.getType().equals(TransactionType.INDIVIDUALINCOME)){
                        expense = expense - t.getAmount();
                    }else{
                        expense = expense + t.getAmount();
                    }
                }
            }
        }

        return expense;
    }

    private void setFilterSpinner(){
        ArrayList<String> types = new ArrayList<>();
        types.add("Purchase");
        types.add("Individual income");
        types.add("Individual payment");
        types.add("Regular income");
        types.add("Regular payment");

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(),android.R.layout.simple_spinner_item,types);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerType.setAdapter(adapter);

        spinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(parent.getItemAtPosition(position).equals("Regular income") || parent.getItemAtPosition(position).equals("Regular payment")){
                    editInterval.setEnabled(true);
                    editEndDate.setEnabled(true);
                }else{
                    editInterval.setEnabled(false);
                    editEndDate.setEnabled(false);
                }

                if(firstTime){
                    firstTime=false;
                }else{
                    if(parent.getItemAtPosition(position).equals("Purchase")){
                        icon.setImageResource(R.drawable.purchase);
                    }else if(parent.getItemAtPosition(position).equals("Individual income")){
                        icon.setImageResource(R.drawable.individualincome);
                    }else if(parent.getItemAtPosition(position).equals("Individual payment")){
                        icon.setImageResource(R.drawable.individualpayment);
                    }else if(parent.getItemAtPosition(position).equals("Regular income")){
                        icon.setImageResource(R.drawable.regularincome);
                    }else{
                        icon.setImageResource(R.drawable.regularpayment);
                    }

                    spinnerType.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                    icon.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void addTransaction(){
        t.setInternalId(transactionListPresenter.getInteractor().getInternalIdFromTransaction(t));

        Transaction replacement = readTransaction();

        clearBackground();

        if(transactionNumber>-1){
            transactionListPresenter.getInteractor().getTransactions().remove(t);
            textMode.setText("Offline\nedit");
        }else{
            replacement.setMode(TransactionDBOpenHelper.TRANSACTION_ADDED);
        }
        transactionListPresenter.getInteractor().getTransactions().add(replacement);

        transactionListPresenter.addOrEditTransaction(replacement);

        t = replacement;

        accountPresenter.getInteractor().getAccount().setBudget(oldBudget);
        accountPresenter.updateAccount(oldBudget, accountPresenter.getInteractor().getAccount().getTotalLimit(),
                accountPresenter.getInteractor().getAccount().getMonthLimit());
    }

    private Calendar dateFromString(String s){
        String[] spl = s.split("\\.");
        int day = Integer.parseInt(spl[0]);
        int month = Integer.parseInt(spl[1]) - 1;
        int year = Integer.parseInt(spl[2]);

        Calendar c = Calendar.getInstance();
        c.set(year,month,day);
        return c;
    }

    private void clearBackground(){
        icon.setBackgroundColor(getResources().getColor(android.R.color.background_light));
        editTitle.setBackgroundColor(getResources().getColor(android.R.color.background_light));
        editAmount.setBackgroundColor(getResources().getColor(android.R.color.background_light));
        editDate.setBackgroundColor(getResources().getColor(android.R.color.background_light));
        spinnerType.setBackgroundColor(getResources().getColor(android.R.color.background_light));
        editDescription.setBackgroundColor(getResources().getColor(android.R.color.background_light));
        editInterval.setBackgroundColor(getResources().getColor(android.R.color.background_light));
        editEndDate.setBackgroundColor(getResources().getColor(android.R.color.background_light));
    }

    private void validateData(){
        if(editTitle.getText().toString().equals("")){
            editTitle.setBackgroundColor(getResources().getColor(R.color.colorPink));
            correctData = false;
            return;
        }
        if(editAmount.getText().toString().equals("")){
            editAmount.setBackgroundColor(getResources().getColor(R.color.colorPink));
            correctData = false;
            return;
        }
        if(editDescription.getText().toString().equals("")){
            editDescription.setBackgroundColor(getResources().getColor(R.color.colorPink));
            correctData = false;
            return;
        }

        if(type.equals(TransactionType.REGULARINCOME) || type.equals(TransactionType.REGULARPAYMENT)){
            if(editInterval.getText().toString().equals("")){
                editInterval.setBackgroundColor(getResources().getColor(R.color.colorPink));
                correctData = false;
                return;
            }

            if(dateFromString(editDate.getText().toString()).after(dateFromString(editEndDate.getText().toString()))){
                editDate.setBackgroundColor(getResources().getColor(R.color.colorPink));
                editEndDate.setBackgroundColor(getResources().getColor(R.color.colorPink));
                correctData = false;
                return;
            }
        }
    }

    private int giveNumbeOfMonthTransactions(Transaction t, int month){

        if(t.getType().equals(TransactionType.REGULARPAYMENT) || t.getType().equals(TransactionType.REGULARINCOME)){

            Calendar start = (Calendar) t.getDate().clone();
            Calendar end = (Calendar) t.getEndDate().clone();

            for(Transaction tr:transactionListPresenter.getInteractor().getTransactions()){
                if(tr.getId()==t.getId()){
                    start = (Calendar) tr.getDate().clone();
                }
            }

            while(start.get(Calendar.MONTH) < month){
                start.add(Calendar.DAY_OF_YEAR,t.getTransactionInterval());
            }

            int numberOfMonthTransactions = 0;
            while(start.get(Calendar.MONTH) == month && start.compareTo(end)<0){
                start.add(Calendar.DAY_OF_YEAR,t.getTransactionInterval());
                numberOfMonthTransactions++;
            }

            return numberOfMonthTransactions;
        }

        return 1;
    }

    private int getTotalNumberOfTransactions(Transaction t, boolean takeFirstRegular){

        if(t.getType().equals(TransactionType.REGULARINCOME) || t.getType().equals(TransactionType.REGULARPAYMENT)){

            Calendar start = (Calendar)t.getDate().clone();
            Calendar end = (Calendar)t.getEndDate().clone();

            if(takeFirstRegular){
                for(Transaction tr:transactionListPresenter.getInteractor().getTransactions()){
                    if(tr.getId()==t.getId()){
                        start = (Calendar) tr.getDate().clone();
                    }
                }
            }

            int numberOfTransactions=0;
            while(start.compareTo(end)<=0){
                start.add(Calendar.DAY_OF_YEAR,t.getTransactionInterval());
                numberOfTransactions++;
            }

            return numberOfTransactions;
        }

        return 1;
    }

    private Transaction readTransaction(){
        Transaction newTransaction = new Transaction(dateFromString(editDate.getText().toString()),
                Double.parseDouble(editAmount.getText().toString()),editTitle.getText().toString(),
                type,editDescription.getText().toString(),0,null);

        if(type.equals(TransactionType.REGULARINCOME) || type.equals(TransactionType.REGULARPAYMENT)){
            newTransaction.setTransactionInterval(Integer.parseInt(editInterval.getText().toString()));
            newTransaction.setEndDate(dateFromString(editEndDate.getText().toString()));
        }

        newTransaction.setId(transactionNumber);
        newTransaction.setInternalId(t.getInternalId());

        return newTransaction;
    }
}
