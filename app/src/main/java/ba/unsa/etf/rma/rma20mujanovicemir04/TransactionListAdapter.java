package ba.unsa.etf.rma.rma20mujanovicemir04;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

public class TransactionListAdapter extends ArrayAdapter<Transaction> {
    private Context context;
    private int resource;

    public TransactionListAdapter(@NonNull Context context, int resource, @NonNull List<Transaction> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        String transactionTitle = getItem(position).getTitle();
        String transactionAmount = Double.toString(getItem(position).getAmount());
        TransactionType transactionType = getItem(position).getType();

        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(resource,parent,false);

        ImageView icon = (ImageView) convertView.findViewById(R.id.icon);
        TextView title = (TextView) convertView.findViewById(R.id.title);
        TextView amount = (TextView) convertView.findViewById(R.id.amount);

        title.setText(transactionTitle);
        amount.setText(transactionAmount);

        if(transactionType.equals(TransactionType.PURCHASE)){
            icon.setImageResource(R.drawable.purchase);
        }else if(transactionType.equals(TransactionType.INDIVIDUALINCOME)){
            icon.setImageResource(R.drawable.individualincome);
        }else if(transactionType.equals(TransactionType.INDIVIDUALPAYMENT)){
            icon.setImageResource(R.drawable.individualpayment);
        }else if(transactionType.equals(TransactionType.REGULARINCOME)){
            icon.setImageResource(R.drawable.regularincome);
        }else{
            icon.setImageResource(R.drawable.regularpayment);
        }

        return convertView;
    }

    public Transaction getTransaction(int position){
        return getItem(position);
    }
}
