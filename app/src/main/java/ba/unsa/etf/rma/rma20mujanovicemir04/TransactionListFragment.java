package ba.unsa.etf.rma.rma20mujanovicemir04;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;

import java.lang.invoke.ConstantCallSite;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

public class TransactionListFragment extends Fragment {

    private TransactionListPresenter transactionListPresenter;
    private AccountPresenter accountPresenter;
    private TextView month;
    private ImageButton backBtn;
    private ImageButton forwardBtn;
    private ListView transactionList;
    private Spinner spinnerFilter;
    private Spinner spinnerSort;
    private Button addTransactionBtn;
    private ConstraintLayout listLayout;

    private TransactionListAdapter adapter;
    private FilterSpinnerAdapter filterAdapter;

    private final SimpleDateFormat monthAndYear = new SimpleDateFormat("MMMM, yyyy");
    public static Calendar calendar = Calendar.getInstance();

    private TextView globalAmount;
    private TextView limit;

    private boolean firstTimeSort = true;
    private boolean firstTimeFilter = true;

    public static boolean noAccountInDB = false;

    public interface OnItemClick {
        void onItemClicked(Transaction transaction);
    }

    public interface OnAddBtnClick {
        void onAddBtnClicked(Calendar c);
    }

    public interface OnMonthChange {
        void onMonthChanged();
    }

    public interface OnLeftSwipe {
        void onLeftSwiped(Fragment f);
    }

    public interface OnRightSwipe {
        void onRightSwiped(Fragment f);
    }

    private OnItemClick oic;
    private OnAddBtnClick oabc;
    private OnMonthChange omc;
    private OnLeftSwipe ols;
    private OnRightSwipe ors;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.transaction_list,container,false);

        accountPresenter = new AccountPresenter(getContext(), new AccountInteractor(getContext()));
        transactionListPresenter = new TransactionListPresenter(getContext(), new TransactionListInteractor(getContext()));

        limit = (TextView)view.findViewById(R.id.limit);
        globalAmount = (TextView)view.findViewById(R.id.globalAmount);
        month = (TextView)view.findViewById(R.id.month);
        backBtn = (ImageButton)view.findViewById(R.id.backBtn);
        forwardBtn = (ImageButton)view.findViewById(R.id.forwardBtn);
        transactionList = (ListView)view.findViewById(R.id.transactionList);
        spinnerFilter = (Spinner)view.findViewById(R.id.spinnerFilter);
        spinnerSort = (Spinner)view.findViewById(R.id.spinnerSort);
        addTransactionBtn = (Button)view.findViewById(R.id.addTransactionBtn);
        listLayout = (ConstraintLayout)view.findViewById(R.id.listLayout);


        transactionList.setOnTouchListener(new OnSwipeTouchListener(getActivity().getApplicationContext()) {
            public void onSwipeTop() {
            }
            public void onSwipeRight() {
                ors.onRightSwiped(TransactionListFragment.this);
            }
            public void onSwipeLeft() {
                ols.onLeftSwiped(TransactionListFragment.this);
            }
            public void onSwipeBottom() {
            }

        });

        listLayout.setOnTouchListener(new OnSwipeTouchListener(getActivity().getApplicationContext()) {
            public void onSwipeTop() {
            }
            public void onSwipeRight() {
                ors.onRightSwiped(TransactionListFragment.this);
            }
            public void onSwipeLeft() {
                ols.onLeftSwiped(TransactionListFragment.this);
            }
            public void onSwipeBottom() {
            }

        });

        setFilterSpinner();
        setSortSpinner();

        month.setText(monthAndYear.format(calendar.getTime()));

        try {
            oic = (TransactionListFragment.OnItemClick) getActivity();
            oabc = (OnAddBtnClick) getActivity();
            omc = (OnMonthChange) getActivity();
            ols = (OnLeftSwipe) getActivity();
            ors = (OnRightSwipe) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() +
                    "Treba implementirati OnItemClick ili OnAddBtnClick");
        }
        transactionList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Transaction t = adapter.getTransaction(position);
                oic.onItemClicked(t);
            }
        });


        if(ConnectivityBroadcastReceiver.isConnected()){
            updateList();
            updateBudget();
        }else{

            readAccountFromDB();


            if(transactionListPresenter.getInteractor().getTransactions()==null){
                readChangesFromDB();
            }

            refreshList();
            refreshBudget();
        }

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar.add(Calendar.MONTH, -1);
                month.setText(monthAndYear.format(calendar.getTime()));
                omc.onMonthChanged();
                if(ConnectivityBroadcastReceiver.isConnected()){
                    updateList();
                }else{
                    refreshList();
                }
            }
        });

        forwardBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar.add(Calendar.MONTH, 1);
                month.setText(monthAndYear.format(calendar.getTime()));
                omc.onMonthChanged();
                if(ConnectivityBroadcastReceiver.isConnected()){
                    updateList();
                }else{
                    refreshList();
                }

            }
        });

        addTransactionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oabc.onAddBtnClicked((Calendar)calendar.clone());
            }
        });

        return view;
    }

    private void readAccountFromDB() {
        Cursor cursor = accountPresenter.getInteractor().getAccountCursor();

        if(cursor.getCount()!=0){
            cursor.moveToFirst();

            int INDEX_BUDGET = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.ACCOUNT_BUDGET);
            int INDEX_TOTAL_LIMIT = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.ACCOUNT_TOTAL_LIMIT);
            int INDEX_MONTH_LIMIT = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.ACCOUNT_MONTH_LIMIT);

            accountPresenter.getInteractor().set(new Account(cursor.getDouble(INDEX_BUDGET), cursor.getDouble(INDEX_TOTAL_LIMIT),
                    cursor.getDouble(INDEX_MONTH_LIMIT)));
        }
    }

    private void readChangesFromDB() {
        Cursor cursor = transactionListPresenter.getInteractor().getTransactionsCursor();

        int INDEX_INTERNAL_ID = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_INTERNAL_ID);
        int INDEX_ID = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ID);
        int INDEX_DATE = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_DATE);
        int INDEX_AMOUNT = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_AMOUNT);
        int INDEX_TITLE = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TITLE);
        int INDEX_TYPE = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TYPE);
        int INDEX_DESCRIPTION = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_DESCRIPTION);
        int INDEX_INTERVAL = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_INTERVAL);
        int INDEX_END_DATE = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_END_DATE);
        int INDEX_MODE = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_MODE);

        transactionListPresenter.getInteractor().set(new ArrayList<Transaction>());

        System.out.println("IMA JEDNA");
        if(cursor.getCount()!=0){
            System.out.println("IMA DRUGA");
        }

        while(cursor.moveToNext()){

            int internalId = cursor.getInt(INDEX_INTERNAL_ID);
            int id = cursor.getInt(INDEX_ID);
            Calendar date = dateFromString(cursor.getString(INDEX_DATE));
            double amount = cursor.getDouble(INDEX_AMOUNT);
            String tite = cursor.getString(INDEX_TITLE);

            int typeInt = cursor.getInt(INDEX_TYPE);

            TransactionType type = getTypeFromId(typeInt);

            String description = cursor.getString(INDEX_DESCRIPTION);
            int interval = cursor.getInt(INDEX_INTERVAL);
            Calendar endDate = null;

            String endDateString = cursor.getString(INDEX_END_DATE);

            if(!(endDateString.equals(""))){
                endDate = dateFromString(endDateString);
            }

            int mode = cursor.getInt(INDEX_MODE);

            Transaction t = new Transaction(date,amount,tite,type,description,interval,endDate);
            t.setId(id);
            t.setMode(mode);
            t.setInternalId(internalId);

            transactionListPresenter.getInteractor().getTransactions().add(t);
        }
    }

    private void updateBudget() {
        accountPresenter.updateAccount();
    }

    private void updateList(){
        transactionListPresenter.updateTransactions();
    }

    private void setFilterSpinner(){
        ArrayList<String> types = new ArrayList<>();
        types.add("All types");
        types.add("Purchase");
        types.add("Individual income");
        types.add("Individual payment");
        types.add("Regular income");
        types.add("Regular payment");

        filterAdapter = new FilterSpinnerAdapter(getContext(),R.layout.filter_spinner_element,types);
        filterAdapter.setDropDownViewResource(R.layout.filter_spinner_element);

        spinnerFilter.setAdapter(filterAdapter);

        spinnerFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(!firstTimeFilter){
                    updateList();
                }else{
                    firstTimeFilter = false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setSortSpinner(){
        ArrayList<String> sortTypes = new ArrayList<>();

        sortTypes.add("Price - Ascending");
        sortTypes.add("Price - Descending");
        sortTypes.add("Title - Ascending");
        sortTypes.add("Title - Descending");
        sortTypes.add("Date - Ascending");
        sortTypes.add("Date - Descending");

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(),android.R.layout.simple_spinner_item,sortTypes);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerSort.setAdapter(adapter);

        spinnerSort.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(!firstTimeSort){
                    updateList();
                }else{
                    firstTimeSort = false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void notifyDataSetChanged(){
        adapter.notifyDataSetChanged();
    }

    public void refresh(){
        updateList();
        updateBudget();
    }

    public void disableAddButton(){
        addTransactionBtn.setEnabled(false);
    }

    public void enableAddButton(){
        addTransactionBtn.setEnabled(true);
    }

    public void enableSelector(){
        transactionList.setSelector(R.color.colorGreen);
    }

    public void disableSelector(){
        transactionList.setSelector(R.color.colorBackgroundLight);
    }

    public void refreshList(){
        ArrayList<Transaction> transactions = new ArrayList<>();

        for(Transaction t : transactionListPresenter.getInteractor().getTransactions()){
            if(monthAndYear.format(t.getDate().getTime()).equals(month.getText().toString())){
                if(((String)spinnerFilter.getSelectedItem()).equals("Purchase") && t.getType().equals(TransactionType.PURCHASE) ||
                        ((String)spinnerFilter.getSelectedItem()).equals("Individual income") && t.getType().equals(TransactionType.INDIVIDUALINCOME) ||
                        ((String)spinnerFilter.getSelectedItem()).equals("Individual payment") && t.getType().equals(TransactionType.INDIVIDUALPAYMENT) ||
                        ((String)spinnerFilter.getSelectedItem()).equals("Regular payment") && t.getType().equals(TransactionType.REGULARPAYMENT) ||
                        ((String)spinnerFilter.getSelectedItem()).equals("Regular income") && t.getType().equals(TransactionType.REGULARINCOME) ||
                        ((String)spinnerFilter.getSelectedItem()).equals("All types")){

                    //Za obicne transakcije i za regular transakcije kada se date poklapa
                    if(t.getType().equals(TransactionType.REGULARPAYMENT) || t.getType().equals(TransactionType.REGULARINCOME)){
                        addRegulars(transactions, t);
                    }else{
                        transactions.add(t);
                    }
                }
            }else if(((((String)spinnerFilter.getSelectedItem()).equals("Regular payment") && t.getType().equals(TransactionType.REGULARPAYMENT)) ||
                    (((String)spinnerFilter.getSelectedItem()).equals("Regular income") && t.getType().equals(TransactionType.REGULARINCOME))
                    || (((String)spinnerFilter.getSelectedItem()).equals("All types") && (t.getType().equals(TransactionType.REGULARPAYMENT) || t.getType().equals(TransactionType.REGULARINCOME))))
                    && (monthAndYear.format(t.getEndDate().getTime()).equals(month.getText().toString()) ||
                    calendar.getTime().after(t.getDate().getTime()) && calendar.getTime().before(t.getEndDate().getTime()))){
                //Za regular transakcije

                addRegulars(transactions,t);

            }
        }

        if(((String)spinnerSort.getSelectedItem()).equals("Price - Ascending")){
            Collections.sort(transactions, new Comparator<Transaction>() {
                @Override
                public int compare(Transaction o1, Transaction o2) {
                    return Double.compare(o1.getAmount(),o2.getAmount());
                }
            });
        }else if(((String)spinnerSort.getSelectedItem()).equals("Price - Descending")){
            Collections.sort(transactions, new Comparator<Transaction>() {
                @Override
                public int compare(Transaction o1, Transaction o2) {
                    return Double.compare(o2.getAmount(),o1.getAmount());
                }
            });
        }else if(((String)spinnerSort.getSelectedItem()).equals("Title - Ascending")){
            Collections.sort(transactions, new Comparator<Transaction>() {
                @Override
                public int compare(Transaction o1, Transaction o2) {
                    return o1.getTitle().compareTo(o2.getTitle());
                }
            });
        }else if(((String)spinnerSort.getSelectedItem()).equals("Title - Descending")){
            Collections.sort(transactions, new Comparator<Transaction>() {
                @Override
                public int compare(Transaction o1, Transaction o2) {
                    return o2.getTitle().compareTo(o1.getTitle());
                }
            });
        }else if(((String)spinnerSort.getSelectedItem()).equals("Date - Ascending")){
            Collections.sort(transactions, new Comparator<Transaction>() {
                @Override
                public int compare(Transaction o1, Transaction o2) {
                    return o1.getDate().getTime().compareTo(o2.getDate().getTime());
                }
            });
        }else{
            Collections.sort(transactions, new Comparator<Transaction>() {
                @Override
                public int compare(Transaction o1, Transaction o2) {
                    return o2.getDate().getTime().compareTo(o1.getDate().getTime());
                }
            });
        }

        adapter = new TransactionListAdapter(getContext(),R.layout.transaction_list_element,transactions);
        transactionList.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public void refreshBudget(){
        globalAmount.setText(Double.toString(accountPresenter.getInteractor().getAccount().getBudget()));
        limit.setText(Double.toString(accountPresenter.getInteractor().getAccount().getTotalLimit()));
    }

    private int giveNumbeOfMonthTransactions(Transaction t, int month, int year) {
        if(t.getType().equals(TransactionType.REGULARPAYMENT) || t.getType().equals(TransactionType.REGULARINCOME)){
            Calendar start = (Calendar) t.getDate().clone();
            Calendar end = (Calendar) t.getEndDate().clone();

            while(start.get(Calendar.YEAR) < year){
                start.add(Calendar.DAY_OF_YEAR,t.getTransactionInterval());
            }

            while(start.get(Calendar.MONTH) < month){
                start.add(Calendar.DAY_OF_YEAR,t.getTransactionInterval());
            }

            int numberOfMonthTransactions = 0;

            while(start.get(Calendar.MONTH) == month && start.get(Calendar.YEAR) == year && start.compareTo(end)<0){
                start.add(Calendar.DAY_OF_YEAR,t.getTransactionInterval());
                numberOfMonthTransactions++;
            }

            return numberOfMonthTransactions;
        }

        return 1;
    }

    private void addRegulars(ArrayList<Transaction> transactions, Transaction t){
        int numberOfMonthTransactions = giveNumbeOfMonthTransactions(t, calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR));

        Calendar before = (Calendar) calendar.clone();

        before.add(Calendar.MONTH, -1);
        int transactionsBefore = 0;

        int monthTransactionsBefore = giveNumbeOfMonthTransactions(t, before.get(Calendar.MONTH), before.get(Calendar.YEAR));
        while(monthTransactionsBefore!=0){
            transactionsBefore = transactionsBefore + monthTransactionsBefore;
            before.add(Calendar.MONTH,-1);
            monthTransactionsBefore = giveNumbeOfMonthTransactions(t, before.get(Calendar.MONTH), before.get(Calendar.YEAR));
        }

        for(int i=0; i<numberOfMonthTransactions; i++){
            Transaction transactionDuplicate = new Transaction((Calendar)t.getDate().clone(), t.getAmount(), t.getTitle(), t.getType(), t.getItemDescription(),
                    t.getTransactionInterval(), t.getEndDate());
            transactionDuplicate.setId(t.getId());
            transactionDuplicate.setMode(t.getMode());

            transactionDuplicate.getDate().add(Calendar.DAY_OF_YEAR, (transactionsBefore + i)*transactionDuplicate.getTransactionInterval());

            transactions.add(transactionDuplicate);
        }
    }

    private Calendar dateFromString(String s){
        String[] spl = s.split("\\.");
        int day = Integer.parseInt(spl[0]);
        int month = Integer.parseInt(spl[1]) - 1;
        int year = Integer.parseInt(spl[2]);

        Calendar c = Calendar.getInstance();
        c.set(year,month,day);
        return c;
    }

    private TransactionType getTypeFromId(int transactionTypeId) {
        while(TransactionType.transactionTypeHashMap==null){
            // Da malo sacekam ako bude trebalo cekati da se transakcijski tipovi pokupe sa web servisa
            // Jer se GET prvo radi, pa je moguce da uhavtim hashmapu dok je null
        }
        return TransactionType.transactionTypeHashMap.get(transactionTypeId);
    }
}

