package ba.unsa.etf.rma.rma20mujanovicemir04;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class TransactionListInteractor extends AsyncTask<Object, Integer, Void> {
    private Context context;

    private String ROOT = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com";
    private String api_id;
    private String mode;

    private ArrayList<Transaction> transactions;
    private SimpleDateFormat joda = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    /*
    public ArrayList<Transaction> get(){
        if(TransactionsModel.transactions == null){
            TransactionsModel.addData();
        }

        return TransactionsModel.transactions;
    }

     */

    public ArrayList<Transaction> getTransactions(){
        return TransactionsModel.transactions;
    }

    public void set(ArrayList<Transaction> transactions){
        TransactionsModel.transactions = transactions;
    }


    public interface OnTransactionsDone{
        public void onDone();
    }

    private OnTransactionsDone caller;

    public TransactionListInteractor(Context context){
        this.context = context;
        api_id = context.getResources().getString(R.string.api_id);
    }

    public TransactionListInteractor(Context context, OnTransactionsDone p){
        this.context = context;
        api_id = context.getResources().getString(R.string.api_id);
        caller = p;
        transactions = new ArrayList<>();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if(mode.equals("get")){
            set(transactions);
            caller.onDone();
        }else if(mode.equals("delete")){

        }else if(mode.equals("add")){

        }else if(mode.equals("edit")){

        }
    }

    @Override
    protected Void doInBackground(Object... objects) {

        String mode = (String) objects[0];
        this.mode = mode;

        if(objects.length==1){
            if(mode.equals("get")){
                webServiceGet();
            }
        }else{
            Transaction t = (Transaction) objects[1];

            if(mode.equals("add")){
                webServiceAdd(t);
            }else if(mode.equals("edit")){
                webServiceEdit(t);
            }else if(mode.equals("delete")){
                webServiceDelete(t);
            }
        }

        return null;
    }

    public int getInternalIdFromTransaction(Transaction t){
        Cursor cur = getTransactionsCursor();

        while(cur.moveToNext()){
            int INDEX_INTERNAL_ID = cur.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_INTERNAL_ID);
            int INDEX_ID = cur.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ID);
            int INDEX_TITLE = cur.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TITLE);

            int internalId = cur.getInt(INDEX_INTERNAL_ID);
            int id = cur.getInt(INDEX_ID);
            String title = cur.getString(INDEX_TITLE);

            if(t.getId() == id){
                if(id == 0){

                    if(t.getTitle().equals(title)){
                        return internalId;
                    }
                }else{
                    return internalId;
                }
            }
        }

        return 0;
    }

    public Cursor getTransactionsCursor(){
        ContentResolver cr = context.getApplicationContext().getContentResolver();
        String[] kolone = new String[]{
                TransactionDBOpenHelper.TRANSACTION_INTERNAL_ID,
                TransactionDBOpenHelper.TRANSACTION_ID,
                TransactionDBOpenHelper.TRANSACTION_DATE,
                TransactionDBOpenHelper.TRANSACTION_AMOUNT,
                TransactionDBOpenHelper.TRANSACTION_TITLE,
                TransactionDBOpenHelper.TRANSACTION_TYPE,
                TransactionDBOpenHelper.TRANSACTION_DESCRIPTION,
                TransactionDBOpenHelper.TRANSACTION_INTERVAL,
                TransactionDBOpenHelper.TRANSACTION_END_DATE,
                TransactionDBOpenHelper.TRANSACTION_MODE
        };
        Uri adresa = Uri.parse("content://rma.provider.transactions/elements");
        String where = null;
        String whereArgs[] = null;
        String order = null;
        Cursor cur = cr.query(adresa,kolone,where,whereArgs,order);
        return cur;
    }
    
    private void webServiceGet() {
        String url = setUrl();

        try {
            int length;
            int pageNum = 0;

            do{
                String realUrl = url + "?page=" + pageNum;

                URL objectURL = new URL(realUrl);
                HttpURLConnection urlConnection = null;
                urlConnection = (HttpURLConnection) objectURL.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String result = convertStreamToString(in);

                JSONObject res = new JSONObject(result);

                JSONArray trans = res.getJSONArray("transactions");

                length = trans.length();

                for(int i = 0; i < length; i++){
                    JSONObject transactionJSON = trans.getJSONObject(i);

                    Transaction t = getTransactionFromJSON(transactionJSON);
                    transactions.add(t);
                }
                pageNum++;
            }while(length==5);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void webServiceDelete(Transaction t) {
        String url = setUrl() + "/" + t.getId();

        System.out.println(url);

        URL objectURL = null;
        try {
            objectURL = new URL(url);
            HttpURLConnection urlConnection = null;
            urlConnection = (HttpURLConnection) objectURL.openConnection();
            urlConnection.setRequestMethod("DELETE");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setDoOutput(true);

            System.out.println(urlConnection.getResponseCode());

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void webServiceAdd(Transaction t) {
        String url = setUrl();
        transactionPOST(t,url);
    }

    private void webServiceEdit(Transaction t) {
        String url = setUrl() + "/" + t.getId();
        transactionPOST(t, url);
    }

    private void transactionPOST(Transaction t, String url){
        try {
            URL objectURL = new URL(url);

            HttpURLConnection urlConnection = null;
            urlConnection = (HttpURLConnection) objectURL.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            urlConnection.setRequestProperty("Accept", "application/json");

            urlConnection.setDoOutput(true);

            String jsonInputString = "{\"date\":" + "\"" + joda.format(t.getDate().getTime()) + "\"" +
                    ",\"title\":" + "\"" + t.getTitle() + "\"" +
                    ",\"amount\":" + t.getAmount() +
                    ",\"TransactionTypeId\":" + t.getType().getValue(t.getType()) +
                    ",\"itemDescription\":" + "\"" + t.getItemDescription() + "\"";

            if(t.getTransactionInterval()==0){
                jsonInputString = jsonInputString + "}";
            }else{
                jsonInputString = jsonInputString + ",\"transactionInterval\":" + t.getTransactionInterval() +
                        ",\"endDate\":" + "\"" + joda.format(t.getEndDate().getTime()) + "\"" + "}";
            }

            try(OutputStream os = urlConnection.getOutputStream()) {
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            try(BufferedReader br = new BufferedReader(
                    new InputStreamReader(urlConnection.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                //System.out.println("RESPONSE" + response.toString());
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String setUrl() {
        String url = ROOT + "/account/" + api_id + "/transactions";
        return url;
    }

    private Transaction getTransactionFromJSON(JSONObject transactionJSON) {
        Transaction t = null;

        try {
            int id = transactionJSON.getInt("id");
            String date = transactionJSON.getString("date");
            Calendar c1 = getDateFromJSONString(date);

            String title = transactionJSON.getString("title");
            double amount = transactionJSON.getDouble("amount");
            String itemDescription = transactionJSON.optString("itemDescription","");
            int transactionInterval = transactionJSON.optInt("transactionInterval",0);


            String endDate = transactionJSON.optString("endDate",null);
            int transactionTypeId = transactionJSON.getInt("TransactionTypeId");
            TransactionType type = getTypeFromId(transactionTypeId);

            Calendar c2 = null;
            if(!(endDate.equals("null"))){
                c2 = getDateFromJSONString(endDate);
            }

            t = new Transaction(c1, amount, title, type, itemDescription, transactionInterval, c2);
            t.setId(id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return t;
    }

    private TransactionType getTypeFromId(int transactionTypeId) {
        while(TransactionType.transactionTypeHashMap==null){
            // Da malo sacekam ako bude trebalo cekati da se transakcijski tipovi pokupe sa web servisa
            // Jer se GET prvo radi, pa je moguce da uhavtim hashmapu dok je null
        }
        return TransactionType.transactionTypeHashMap.get(transactionTypeId);
    }

    private Calendar getDateFromJSONString(String date) {
        int year = Integer.parseInt(date.substring(0,4));
        int month = Integer.parseInt(date.substring(5,7)) - 1;
        int day = Integer.parseInt(date.substring(8,10));
        int hour = Integer.parseInt(date.substring(11,13));
        int minutes = Integer.parseInt(date.substring(14,16));
        int seconds = Integer.parseInt(date.substring(17,19));

        Calendar c1 = Calendar.getInstance();
        c1.set(year,month,day,hour,minutes,seconds);

        return c1;
    }

    private String convertStreamToString(InputStream in) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                in.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }
}
