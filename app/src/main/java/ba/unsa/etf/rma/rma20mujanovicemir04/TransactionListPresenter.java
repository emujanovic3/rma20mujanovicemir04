package ba.unsa.etf.rma.rma20mujanovicemir04;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;

import java.util.ArrayList;
import java.util.Calendar;

public class TransactionListPresenter implements TransactionListInteractor.OnTransactionsDone {
    private Context context;
    private TransactionListInteractor interactor;

    public TransactionListPresenter(Context context, TransactionListInteractor interactor) {
        this.context = context;
        this.interactor = interactor;
    }

    public void updateTransactions() {
        if(ConnectivityBroadcastReceiver.isConnected()){
            new TransactionListInteractor(context, (TransactionListInteractor.OnTransactionsDone)this).execute("get");
        }
    }

    public void addOrEditTransaction(Transaction t) {
        if(ConnectivityBroadcastReceiver.isConnected()){
            if(t.getId()==-1 || t.getId()==0){
                new TransactionListInteractor(context, (TransactionListInteractor.OnTransactionsDone)this).execute("add",t);
            }else{
                new TransactionListInteractor(context, (TransactionListInteractor.OnTransactionsDone)this).execute("edit",t);
            }
        }else{
            if(t.getId()==-1){
                t.setId(0);
                if(t.getInternalId()==0){
                    insertTransactionToDB(t, TransactionDBOpenHelper.TRANSACTION_ADDED);
                }else{
                    updateTransactionToDB(t, TransactionDBOpenHelper.TRANSACTION_ADDED);
                }
            }else{
                if(t.getInternalId()==0){
                    insertTransactionToDB(t, TransactionDBOpenHelper.TRANSACTION_EDITED);
                }else{
                    updateTransactionToDB(t, TransactionDBOpenHelper.TRANSACTION_EDITED);
                }
            }
        }
    }

    public void deleteTransaction(Transaction t) {
        if(ConnectivityBroadcastReceiver.isConnected()){
            new TransactionListInteractor(context, (TransactionListInteractor.OnTransactionsDone)this).execute("delete",t);
        }else{
            if(t.getInternalId()==0){
                insertTransactionToDB(t, TransactionDBOpenHelper.TRANSACTION_DELETED);
            }else{
                updateTransactionToDB(t, TransactionDBOpenHelper.TRANSACTION_DELETED);
            }

            //Ako je vec u offline i zelimo neku editovanu transakciju da obrisemo
            getInteractor().getTransactions().remove(t);
            t.setMode(TransactionDBOpenHelper.TRANSACTION_DELETED);
            getInteractor().getTransactions().add(t);
        }

    }

    private void updateTransactionToDB(Transaction t, int mode) {
        ContentResolver cr = context.getApplicationContext().getContentResolver();

        Uri uri = ContentUris.withAppendedId(Uri.parse("content://rma.provider.transactions/elements"),t.getInternalId());

        ContentValues values = new ContentValues();
        values.put(TransactionDBOpenHelper.TRANSACTION_ID, t.getId());

        if(t.getType().equals(TransactionType.REGULARINCOME) || t.getType().equals(TransactionType.REGULARPAYMENT)){
            for(Transaction tr : getInteractor().getTransactions()){
                if(tr.getInternalId() == t.getInternalId() && tr.getId() == t.getId()){
                    t.setDate((Calendar) tr.getDate().clone());
                    break;
                }
            }
        }

        values.put(TransactionDBOpenHelper.TRANSACTION_DATE, t.getDate().get(Calendar.DAY_OF_MONTH) + "." + (t.getDate().get(Calendar.MONTH)+1) + "." + t.getDate().get(Calendar.YEAR));
        values.put(TransactionDBOpenHelper.TRANSACTION_AMOUNT, t.getAmount());
        values.put(TransactionDBOpenHelper.TRANSACTION_TITLE, t.getTitle());
        values.put(TransactionDBOpenHelper.TRANSACTION_TYPE, TransactionType.getValue(t.getType()));
        values.put(TransactionDBOpenHelper.TRANSACTION_DESCRIPTION, t.getItemDescription());
        values.put(TransactionDBOpenHelper.TRANSACTION_INTERVAL, t.getTransactionInterval());

        if(t.getEndDate()!=null){
            values.put(TransactionDBOpenHelper.TRANSACTION_END_DATE, t.getEndDate().get(Calendar.DAY_OF_MONTH) + "." + (t.getEndDate().get(Calendar.MONTH)+1) + "." + t.getEndDate().get(Calendar.YEAR));
        }else{
            values.put(TransactionDBOpenHelper.TRANSACTION_END_DATE, "");
        }

        values.put(TransactionDBOpenHelper.TRANSACTION_MODE, mode);

        int updated = cr.update(uri,values, null, null);

        System.out.println("INTERNAL " + t.getInternalId());
        System.out.println("UPDATED " + updated);
    }

    public void undoDelete(Transaction t) {
        if(t.getId()==0){
            updateTransactionToDB(t, TransactionDBOpenHelper.TRANSACTION_ADDED);
        }else{
            updateTransactionToDB(t, TransactionDBOpenHelper.TRANSACTION_EDITED);
        }
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public TransactionListInteractor getInteractor() {
        return interactor;
    }

    public void setInteractor(TransactionListInteractor interactor) {
        this.interactor = interactor;
    }

    @Override
    public void onDone(){
        MainActivity mainActivity = (MainActivity) context;
        mainActivity.onDone();

        //Vezano za UNDO i listu transakcija prilikom offline
        TransactionDetailFragment.dataFromDB = false;
    }

    public void deleteAllTransactionsFromDB() {
        ContentResolver cr = context.getApplicationContext().getContentResolver();
        Uri uri = Uri.parse("content://rma.provider.transactions/elements");

        cr.delete(uri, null, null);
    }

    private void insertTransactionToDB(Transaction t, int mode){
        ContentResolver cr = context.getApplicationContext().getContentResolver();

        Uri uri = Uri.parse("content://rma.provider.transactions/elements");

        ContentValues values = new ContentValues();
        values.put(TransactionDBOpenHelper.TRANSACTION_ID, t.getId());

        if(t.getType().equals(TransactionType.REGULARINCOME) || t.getType().equals(TransactionType.REGULARPAYMENT)){
            for(Transaction tr : getInteractor().getTransactions()){
                if(tr.getInternalId() == t.getInternalId() && tr.getId() == t.getId()){
                    t.setDate((Calendar) tr.getDate().clone());
                    break;
                }
            }
        }
        values.put(TransactionDBOpenHelper.TRANSACTION_DATE, t.getDate().get(Calendar.DAY_OF_MONTH) + "." + (t.getDate().get(Calendar.MONTH)+1) + "." + t.getDate().get(Calendar.YEAR));
        values.put(TransactionDBOpenHelper.TRANSACTION_AMOUNT, t.getAmount());
        values.put(TransactionDBOpenHelper.TRANSACTION_TITLE, t.getTitle());
        values.put(TransactionDBOpenHelper.TRANSACTION_TYPE, TransactionType.getValue(t.getType()));
        values.put(TransactionDBOpenHelper.TRANSACTION_DESCRIPTION, t.getItemDescription());
        values.put(TransactionDBOpenHelper.TRANSACTION_INTERVAL, t.getTransactionInterval());

        if(t.getEndDate()!=null){
            values.put(TransactionDBOpenHelper.TRANSACTION_END_DATE, t.getEndDate().get(Calendar.DAY_OF_MONTH) + "." + (t.getEndDate().get(Calendar.MONTH)+1) + "." + t.getEndDate().get(Calendar.YEAR));
        }else{
            values.put(TransactionDBOpenHelper.TRANSACTION_END_DATE, "");
        }

        values.put(TransactionDBOpenHelper.TRANSACTION_MODE, mode);

        cr.insert(uri, values);
    }
}

