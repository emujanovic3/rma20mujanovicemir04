package ba.unsa.etf.rma.rma20mujanovicemir04;

import java.util.HashMap;
import java.util.Map;

public enum TransactionType {
    REGULARPAYMENT,
    REGULARINCOME,
    PURCHASE,
    INDIVIDUALINCOME,
    INDIVIDUALPAYMENT;

    public static HashMap<Integer, TransactionType> transactionTypeHashMap;

    public static int getValue(TransactionType t){
        for(Map.Entry<Integer, TransactionType> me : transactionTypeHashMap.entrySet()){
            if(me.getValue().equals(t)){
                return me.getKey();
            }
        }
        return 0;
    }
}
