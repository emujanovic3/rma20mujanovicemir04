package ba.unsa.etf.rma.rma20mujanovicemir04;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import static ba.unsa.etf.rma.rma20mujanovicemir04.TransactionsModel.transactions;

public class TransactionTypeInteractor extends AsyncTask<Void, Integer, Void> {
    private String ROOT = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com";

    private HashMap<Integer, TransactionType> types = new HashMap<>();

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        TransactionType.transactionTypeHashMap = types;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        String realUrl = ROOT + "/transactionTypes";

        URL objectURL = null;
        try {
            objectURL = new URL(realUrl);

            HttpURLConnection urlConnection = null;
            urlConnection = (HttpURLConnection) objectURL.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String result = convertStreamToString(in);

            JSONObject res = new JSONObject(result);

            JSONArray trans = res.getJSONArray("rows");

            int count = trans.length();

            for(int i = 0; i < count; i++){
                JSONObject typeJSON = trans.getJSONObject(i);

                types.put(typeJSON.getInt("id"),
                        getTransactionTypeFromString(typeJSON.getString("name").trim()));
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private TransactionType getTransactionTypeFromString(String name) {
        if(name.equals("Regular payment")){
            return TransactionType.REGULARPAYMENT;
        }else if(name.equals("Regular income")){
            return TransactionType.REGULARINCOME;
        }else if(name.equals("Purchase")){
            return TransactionType.PURCHASE;
        }else if(name.equals("Individual income")){
            return TransactionType.INDIVIDUALINCOME;
        }else{
            return TransactionType.INDIVIDUALPAYMENT;
        }
    }

    private String convertStreamToString(InputStream in) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                in.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }
}
