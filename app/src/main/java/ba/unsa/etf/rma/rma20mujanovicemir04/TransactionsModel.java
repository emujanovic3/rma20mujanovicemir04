package ba.unsa.etf.rma.rma20mujanovicemir04;

import java.util.ArrayList;
import java.util.Calendar;

public class TransactionsModel {
    static ArrayList<Transaction> transactions;

    public TransactionsModel() {
    }

    public static void addData(){
        transactions = new ArrayList<>();


        Calendar c1 = Calendar.getInstance();
        //Mjeseci počinju od nule
        c1.set(2020, 2,1);
        transactions.add(new Transaction(c1,100,"Dug",TransactionType.INDIVIDUALINCOME, "Povrat duga", 0, null));

        Calendar c2 = Calendar.getInstance();
        c2.set(2020,2, 5);
        transactions.add(new Transaction(c2,50,"Kupovina",TransactionType.PURCHASE,"Kupio novi miš",0,null));

        Calendar c3 = Calendar.getInstance();
        c3.set(2020,1,23);
        transactions.add(new Transaction(c3,30,"Rent a car",TransactionType.INDIVIDUALPAYMENT,"Bilo mi je potrebno auto, pa sam ga iznajmio",0,null));



        Calendar c4 = Calendar.getInstance();
        c4.set(2020,0,20);
        Calendar c41 = Calendar.getInstance();
        c41.set(2020,11,30);
        transactions.add(new Transaction(c4,1000,"Plata",TransactionType.REGULARINCOME,"Moja plata",30,c41));


        Calendar c5 = Calendar.getInstance();
        c5.set(2020,3,10);
        Calendar c51 = Calendar.getInstance();
        c51.set(2020,9,10);

        transactions.add(new Transaction(c5,250,"Rata kredita",TransactionType.REGULARPAYMENT,"Vraćanje kredita",30,c51));

        Calendar c6 = Calendar.getInstance();
        c6.set(2020,5,17);
        transactions.add(new Transaction(c6,120,"Kupovina patika",TransactionType.PURCHASE,"Kupio sam nove patika",0,null));


        Calendar c7 = Calendar.getInstance();
        c7.set(2020,3,23);
        transactions.add(new Transaction(c7,250,"Prodao stari telefon",TransactionType.INDIVIDUALINCOME,"Prodao sam stari telefon",0,null));


        Calendar c8 = Calendar.getInstance();
        c8.set(2020,3,25);
        transactions.add(new Transaction(c8,600,"Kupio novi telefon",TransactionType.PURCHASE,"Kupio sam novi telefon",0,null));


        Calendar c9 = Calendar.getInstance();
        c9.set(2020,4,1);
        transactions.add(new Transaction(c9,100,"Dao bratu pare",TransactionType.INDIVIDUALPAYMENT,"Trebalo bratu malo para",0,null));


        Calendar c10 = Calendar.getInstance();
        c10.set(2020,5,10);
        transactions.add(new Transaction(c10,150,"Posuio pare",TransactionType.INDIVIDUALPAYMENT,"Posudio sam pare drugu",0,null));


        Calendar c11 = Calendar.getInstance();
        c11.set(2021,0,4);
        transactions.add(new Transaction(c11,1000,"Kupio novi računar",TransactionType.PURCHASE,"Kupio novi račnar",0,null));


        Calendar c12 = Calendar.getInstance();
        c12.set(2020,8,14);
        transactions.add(new Transaction(c12,150,"Dug vraćen",TransactionType.INDIVIDUALINCOME,"Vraćen dug star 3 mjeseca", 0,null));


        Calendar c13 = Calendar.getInstance();
        c13.set(2020,7,3);
        transactions.add(new Transaction(c13,200,"Izgubio 200KM", TransactionType.INDIVIDUALPAYMENT,"Negdje sam izgubio 200KM",0,null));


        Calendar c14 = Calendar.getInstance();
        c14.set(2020,5,16);
        transactions.add(new Transaction(c14,500,"Rodbina",TransactionType.INDIVIDUALINCOME,"Rodbila bila u posjeti",0,null));


        Calendar c15 = Calendar.getInstance();
        c15.set(2020,7,23);
        transactions.add(new Transaction(c15,100,"Shopping",TransactionType.PURCHASE,"Obukao se haha",0,null));


        Calendar c16 = Calendar.getInstance();
        c16.set(2020,0,4);
        transactions.add(new Transaction(c16, 50,"Humanitarne svrhe",TransactionType.INDIVIDUALPAYMENT,"Dobro djelo",0,null));


        Calendar c17 = Calendar.getInstance();
        c17.set(2020,1,5);
        transactions.add(new Transaction(c17, 1,"Kesa boba",TransactionType.PURCHASE,"Malo bombona haha",0,null));


        Calendar c18 = Calendar.getInstance();
        c18.set(2021,0,1);
        transactions.add(new Transaction(c18, 15,"Kupovina namirnica",TransactionType.PURCHASE,"Kupovina namirnica",0,null));


        Calendar c19 = Calendar.getInstance();
        c19.set(2020,10,19);
        transactions.add(new Transaction(c19,500,"Nagrada",TransactionType.INDIVIDUALINCOME,"Nagrada za fair play",0,null));


        Calendar c20 = Calendar.getInstance();
        c20.set(2020,2,1);
        transactions.add(new Transaction(c20,100,"Nagrada RMA",TransactionType.INDIVIDUALINCOME,"Nagrada za spiralu",0,null));
    }
}
